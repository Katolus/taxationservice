package taxationService.profile;

import com.ibm.b2b.as4.client.jumpstart.*;

import java.util.List;

public class DummyExchangeProfile {

    protected TradingPartnerExchangeProfile exchangeProfile;

    public DummyExchangeProfile(){

        TradingPartnerExchangeProfile tp = new TradingPartnerExchangeProfile();
        tp.setName( "SampleBasicOneWayPull" );
        tp.setPattern( ExchangeProfilePattern.ONE_WAY_PULL );
        tp.setProtocol( ExchangeProfileProtocol.AS_4 );
        tp.setService( "OneWayPullBasic" );
        tp.setAgreementUrl( "AgreementURI" );
        tp.setPModeId( "PmodeId" );
        // creating conformance policy.
        ConformancePolicy cp = new ConformancePolicy();
        cp.setName( "SampleConformancePolicyBasic" );
        cp.setPiggyBackingEnabled( false );

        Security security = new Security();
        security.setSignOutboundMessages( false );
        security.setRequireSignedInboundMessages( false );
        security.setBasicAuthMode( BasicAuthMode.NONE );

        cp.setSecurity( security );
        cp.setErrorHandlingMode( SendingMode.SYNC );
        cp.setReceiptMode( SendingMode.NONE );
        cp.setReliableMessagingEnabled( false );
        cp.setSplitJoinEnabled( false );
        cp.setSrcMsgStorageEnabled( false );
        cp.setSrcMsgSecurityEnabled( false );
        cp.setSrcMsgCompressionEnabled( false );
        cp.setNrrEnabled( false );
        cp.setCompressionEnabled( false );
        cp.setCompressFileSize( 0 );

        tp.setConformancePolicy( cp );

        // setting participants
        Participants partners = new Participants();
        Organization ownerOrg = new Organization();
        ownerOrg.setName( "ORG_A" );
        ownerOrg.setBusinessId( "ORGA" );
        ownerOrg.setRole( "Initiator - Producer" );
        partners.setOwner( ownerOrg );

        Organization tradPartOrg = new Organization();
        tradPartOrg.setName( "MEG Master Org" );
        tradPartOrg.setDescription( "Default Master Organization" );
        tradPartOrg.setBusinessId( "ORGB" );
        tradPartOrg.setRole( "Responder - Consumer" );
        Address address = new Address();
        address.setStreetAddress( "4600 Lakehurst Ct." );
        address.setCity( "Dublin" );
        address.setCountryCode( "US" );
        address.setRegionCode( "OH" );
        address.setPostalCode( "43016" );
        tradPartOrg.setAddress( address );

        tradPartOrg.setPhoneNumber( "5555555" );

        Contacts contacts = new Contacts();
        List<Contact> contactList = contacts.getContact();
        Contact contact = new Contact();
        contact.setName( "Default Master Admin" );
        contact.setPhoneNumber( "5555555" );
        contact.setEmail( "meguser@us.ibm.com" );
        contact.setPrimary( false );
        contactList.add( contact );
        tradPartOrg.setContacts( contacts );

        partners.setTradingPartner( tradPartOrg );

        tp.setParticipants( partners );

        Action reqAction = new Action();
        ProtocolProfile reqPP = new ProtocolProfile();
        SecuritySettings ss = new SecuritySettings();
        ss.setReceiptDestinationSslEnabled( false );
        ss.setReceiptDestinationClientAuthEnabled( false );
        ss.setErrorDestinationSslEnabled( false );
        ss.setErrorDestinationClientAuthEnabled( false );
        reqPP.setSecuritySettings( ss );
        reqAction.setProtocolProfile( reqPP );
        reqAction.setDestinationUrl( "https://test.ato.sbr.gov.au:443/services" );
        reqAction.setDestinationSslEnabled( false );
        reqAction.setDestinationClientAuthEnabled( false );

        tp.setRequest( reqAction );

        Action respAction = new Action();
        respAction.setDestinationSslEnabled( false );
        respAction.setDestinationClientAuthEnabled( false );
        tp.setResponse( respAction );

        exchangeProfile = tp;
    }

    public TradingPartnerExchangeProfile getExchangeProfile() {
        return exchangeProfile;
    }
}
