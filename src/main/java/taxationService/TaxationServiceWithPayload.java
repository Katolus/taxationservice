package taxationService;

import com.ibm.b2b.apiint.messagedefinitions.messages.xml.dispatcher.*;
import com.ibm.b2b.as4.client.*;
import com.ibm.b2b.as4.client.internal.ClientImpl;
import taxationService.message.AttachmentType;
import taxationService.message.RequestFactory;
import taxationService.security.SecurityCheck;
import taxationService.util.PropertiesLoader;

import java.io.IOException;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaxationServiceWithPayload {

    static final Logger LOGGER = Logger.getLogger(TaxationSignalMessageClient.class.getName());
    public final static PropertiesLoader taxationClientPropsLoader = new PropertiesLoader("config/Taxation.properties");

    public static void main(String[] args) {
        final String mainKeyStorePassPhrase = "IGt4a_?6Yui77qwss";
        final String configDirectory = taxationClientPropsLoader.props.getProperty("AS4ClientConfigDirectory", "config");
        final String dataDirectory = taxationClientPropsLoader.props.getProperty("AS4ClientDataDirectory", "data");
        final String auditDirectory = taxationClientPropsLoader.props.getProperty("AS4ClientAuditDirectory", "audit");
        final String payloadPath = taxationClientPropsLoader.props.getProperty("PayLoadDirectory", null);
        final String payloadContentType = taxationClientPropsLoader.props.getProperty("PayLoadContentType", "text/xml");
        final String payload = taxationClientPropsLoader.props.getProperty("Payload", null);
        final String amplifyPayload = taxationClientPropsLoader.props.getProperty("AmplifyPayload", null);
        final String attachmentPath = taxationClientPropsLoader.props.getProperty("AttachmentDirectory", null);
        final String profileName = "SingleSync-EVTE";
        final String certificateAlias = "ABRP:27809366375_10000022";

        try (AS4Client as4Client = AS4ClientFactory.getClient(configDirectory, dataDirectory, mainKeyStorePassPhrase,
                auditDirectory)) {

            RequestFactory requestFactory = new RequestFactory(profileName, as4Client);
            requestFactory.createRequest(certificateAlias);
            requestFactory.constructUserMessage(payloadPath, payloadContentType, payload, amplifyPayload, attachmentPath);

            Request request = requestFactory.getRequest();

            LOGGER.log(Level.INFO, "Sending request...");
            Response response = request.send();
            LOGGER.log(Level.INFO, "Response received.");

        } catch (AS4ClientIllegalRequestException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4ClientIllegalRequestException has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (AS4ClientException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4Client has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "An IOException occurred while creating signal message.");
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An unknown exception has occurred.");
            e.printStackTrace();
        }
    }

}
