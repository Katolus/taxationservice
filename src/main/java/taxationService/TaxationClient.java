package taxationService;

import au.gov.abr.securitytokenmanager.SecurityToken;
import au.gov.abr.securitytokenmanager.exceptions.STMException;
import com.ibm.b2b.as4.client.*;
import taxationService.message.*;
import taxationService.security.AUSKeyHandler;
import taxationService.util.PropertiesLoader;
import taxationService.util.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaxationClient {

    private static boolean secureMessagingFlag = false;

    final static String PAYLOAD_BASE_PATH = "Payload";
    final static String ATTACHMENTS_BASE_PATH = "Attachments";
    final static String ASYNC_ID_FOLDER_NAME = "async_ids";
    // User to access the keyStore while creating an AS4 client
    private static String mainKeyStorePassPhrase = "IGt4a_?6Yui77qwss";

    public static void main(String[] args) {
//        if (args.length == 0) {
//            printUsage();
//            return;
//        }
//        Map<Option, String> options = getOptions(args);

        final Logger LOGGER = Logger.getLogger(TaxationClient.class.getName());
        LOGGER.log(Level.INFO, "Starting TaxationClient " + LocalDateTime.now());

        boolean logInPerformance = System.getProperties().getProperty("referenceClient.logPerformanceStats") != null;
        LOGGER.log(Level.INFO, "Program will keep performance logs -> " + logInPerformance);

        // For performanceLogs
        final String refClientStartDate = LocalDateTime.now().toString();

        final String parentDirectory = Utility.getParentDirectory(TaxationClient.class);
        final String configDirectory = "config";
        final String ausKeyStorePassword = "Password1!";
        secureMessagingFlag = Boolean.parseBoolean("false");
        final String aType = "SRP";
        String enableAudit = "true";
        final String profileName = "SingleSync-EVTE";
        final String certificateAlias = "ABRD:98089681872_RevokedDevice";
        final String vanguardServiceType = "test";
        final String overrideOwnerOrgName = "Katoliks";
        final String overrideOwnerOrgRole = "http://sbr.gov.au/ato/Role/Registered Agent";
        final String overrideOwnerOrgBusinessId = "17801003";
        final String overrideOwnerOrgBusinessIdType = "http://ato.gov.au/PartyIdType/TAN";
//
//        // This looks terrible, need to be optimised
//        final String profileName = getOption(Option.EXCHANGE_PROFILE, options);
//        final String requestDirectory = getOption(Option.REQUEST_FOLDER, options);
//        final String certificateAlias = getOption(Option.CERTIFICATE_ALIAS, options);
//
//        final String ausKeyStorePassword = getOption(Option.AUSKEY_PASSPHRASE, options);
//        String secureMessagingFlag = getOption(Option.SAML_FLAG, options);
//        final String vanguardServiceType = getOption(Option.VANGUARD_ENDPOINT, options);
//        final String overrideOwnerOrgName = getOption(Option.OVERRIDE_OWNER_ORGANIZATION_NAME, options);
//        final String overrideOwnerOrgRole = getOption(Option.OVERRIDE_OWNER_ORGANIZATION_ROLE, options);
//        final String overrideOwnerOrgBusinessId = getOption(Option.OVERRIDE_OWNER_ORGANIZATION_BUSINESS_ID, options);
//        final String overrideOwnerOrgBusinessIdType = getOption(Option.OVERRIDE_OWNER_ORGANIZATION_BUSINESS_ID_TYPE, options);
//        final String aType = getOption(Option.ATTACHMENT_TYPE, options);
//        String enableAudit = getOption(Option.ENABLE_AUDIT, options);

        final String requestBasePath = "requests";
        final String attachmentsDirectory = "attachments";
        final String messageIDFilePath = ASYNC_ID_FOLDER_NAME + "/" + requestBasePath;
        final String samlTokenPath = parentDirectory + "/" + "savedSAML" + "/" + certificateAlias.substring(5);

        LOGGER.log(Level.INFO, "Parent Directory for File Locations: " + parentDirectory);
        LOGGER.log(Level.INFO, "AttachmentType: " + aType);

        AttachmentType attachmentType = null;
        if (aType != null) {
            attachmentType = AttachmentType.fromString(aType);
        }

        PropertiesLoader taxationClientPropsLoader = new PropertiesLoader("config/Taxation.properties");
        String ausKeyStart = "";
        String samlend = "";

        // Security protocols
        String samlToken = null;
        PrivateKey privateKey = null;
        X509Certificate[] certChain = null;

        // Whether the connection is secure need to be secure or not
        if (secureMessagingFlag) {
            LOGGER.log(Level.INFO, "Creating AUSKey certificates at " + LocalDateTime.now());
            // Retrieve X509 certificate from AUSKey Store
            ausKeyStart = LocalDateTime.now().toString();
            String ausKeyLocation = "";
            String ausKeyPropsFile = taxationClientPropsLoader.props.getProperty("AusKeystore-Location");
            String samlExpiryString = taxationClientPropsLoader.props.getProperty("Saml-Valid-For");
            if (ausKeyPropsFile != null && !ausKeyPropsFile.isEmpty()) {
                ausKeyLocation = ausKeyPropsFile;
            } else {
                ausKeyLocation = parentDirectory + File.separator + "KeyStore.xml";
                LOGGER.log(Level.INFO, "Failed to log configuration keyStore from the properties file");
            }
            LOGGER.log(Level.INFO, String.format(" ### TaxationClient key property AUSKey file %s and AUSKeyPassword %s", ausKeyPropsFile, ausKeyStorePassword));
            // Used to read configuration files regarding SecurityToken and get that Token from the SBR instance depending what endPoint URL is provided
            AUSKeyHandler ausKeyHandler = new AUSKeyHandler(ausKeyStorePassword, ausKeyLocation);
            privateKey = ausKeyHandler.getPrivateKey(certificateAlias);
            certChain = ausKeyHandler.getCertificateChain(certificateAlias);

            LOGGER.log(Level.INFO, "SAML Expiry Duration " + samlExpiryString);
            // If there is SAML Token already, reuse it (stored in properties file -> samlTokenPath
            try {
                samlToken = Utility.reuseSAMLToken(samlTokenPath, Integer.valueOf(samlExpiryString));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (samlToken == null) {
                LOGGER.log(Level.INFO,
                        "TaxationClient requesting samlToken  " + LocalDateTime.now());
                try {
                    // Retrieve SAML Token from VanGuard
                    String serviceEndpoint = taxationClientPropsLoader.props.getProperty("Service_Endpoint", "https://test.sbr.gov.au/services");
                    SecurityToken secToken = ausKeyHandler.getSAMLToken(certificateAlias, certChain, privateKey, vanguardServiceType, serviceEndpoint);
                    samlToken = secToken.getAssertionAsString();
                    Utility.writeString2File(samlToken, samlTokenPath);
                } catch (STMException e1) {
                    LOGGER.log(Level.SEVERE, "Unable to retrieve SAML token from STS Response at: " + LocalDateTime.now());
                    e1.printStackTrace();
                } catch (FileNotFoundException e) {
                    LOGGER.log(Level.SEVERE, "An error occurred while writing the SAMLToken to a file");
                    e.getMessage();
                    e.printStackTrace();
                }
                LOGGER.log(Level.INFO, "### TaxationClient received samlToken  " + LocalDateTime.now());
                LOGGER.log(Level.INFO, "Start SAML Token ----------------------------------");
                LOGGER.log(Level.INFO, samlToken);
                LOGGER.log(Level.INFO, "End SAML Token ---------------------------------- at: " + LocalDateTime.now());
            } else {
                LOGGER.log(Level.INFO, "### TaxationClient reusing existing SAML token " + LocalDateTime.now());
            }
        }

        // End of Security Token generation

        /**
         * Get the comment out of the PerformanceTestID folder if the
         * performance test logging is enabled
         **/

        StringJoiner performanceLogString = null;
        if (logInPerformance) {
            performanceLogString = new StringJoiner("");
            performanceLogString
                    .add("RC_Comment,RC_MessageID,RC_Invoke_Time,RC_API_Invoke_Time,RC_Resp_Rec_Time,RC_AUSKEY_Invoke,RC_SAML_Resp");
            performanceLogString.add(System.getProperty("line.separator"));
            // Need to adjust the path
            performanceLogString.add(Utility.readFirstLineFromFile(requestBasePath + File.separator + "PerformanceTestId.txt"));
            performanceLogString.add(",");
        }

        /**
         * No need to instantiate payload as Message payload i.e. Soap Body will
         * be empty for SBR ebMS3 requests, see SBR2 WEb Services Implementation
         * Guide (WIG) for details
         **/

        // check the auditEnable flag to decide if audit logging should be enabled
        if (enableAudit == null) {
            enableAudit = "true";
        }
        String auditFolderLocation = null;
        if (Boolean.parseBoolean(enableAudit)) {
            auditFolderLocation = "audit";
            Path auditFolderPath = Paths.get(Utility.encodeURL(auditFolderLocation));
            LOGGER.log(Level.INFO, auditFolderLocation);
            if (Files.notExists(auditFolderPath)) {
                LOGGER.log(Level.INFO, "Creating audit directory");
                try {
                    Files.createDirectory(auditFolderPath);
                    LOGGER.log(Level.INFO, "Directory exist -> " + Files.exists(auditFolderPath));
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Unable to create the directory under this location -> " + auditFolderPath);
                    e.printStackTrace();
                }
            } else {
                LOGGER.log(Level.INFO, "Audit directory already created");
            }
        }
        LOGGER.log(Level.INFO, "Audit folder location: " + auditFolderLocation);
        LOGGER.log(Level.INFO, "TaxationClient create request " + LocalDateTime.now());

        Request requestMessage = null;
        LOGGER.log(Level.INFO, "messagePayLoad location ->  " + "attachments/payload.dat" + " exists -> " + Utility.doesAFileExistAsAResource("attachments/payload.dat"));

//        MessagePayload msgPayload = new MessagePayload("attachments/payload.dat");
        MessagePayload msgPayload = null;
        MessageAttachments msgAttachments = null;
        RequestFactory requestFactory = new RequestFactory(profileName);

        // Main communication client, reads configuration files from the config folder
        final String dataDirectory = "data";
        final String responseDirectory = "response";
        try (AS4Client client = AS4ClientFactory.getClient(configDirectory, dataDirectory, mainKeyStorePassPhrase, auditFolderLocation)) {

            requestMessage = requestFactory.getRequest(requestBasePath, overrideOwnerOrgName, overrideOwnerOrgRole,
                    overrideOwnerOrgBusinessId, overrideOwnerOrgBusinessIdType, client);

            // Maybe not static?
            InteractionType interactionType = InteractionType.fromString(requestFactory.getProfile().getPattern());
//            msgAttachments = new MessageAttachments(attachmentsDirectory, interactionType);
            LOGGER.log(Level.INFO, "Interaction Type: " + interactionType.getInteractionType());

            if (interactionType == InteractionType.TWO_WAY_SYNC
                    || interactionType == InteractionType.ONE_WAY_PUSH
                    || interactionType == InteractionType.TWO_WAY_PUSH_PULL) {

                LOGGER.log(Level.INFO, "Creating attachments at: " + LocalDateTime.now());
                // Not Required for SBR ebMS3 messages
//                msgPayload.addPayload(requestMessage);

                // Create part
                LOGGER.log(Level.INFO, "Attachment Path: " + attachmentsDirectory);
//                msgAttachments.attachParts(requestMessage, attachmentType);

                if (samlToken != null) {
                    LOGGER.log(Level.INFO, "TaxationClient setting SAML " + LocalDateTime.now());
                    requestMessage.getUserMessage().setSAMLToken(samlToken.toString());
                } else {
                    LOGGER.log(Level.INFO, "Skipping request SAML Token as either the secureMessaging flag is set to false or the retrieved SAML token is null.");
                }
            } else if (interactionType == InteractionType.ONE_WAY_SELECT_PULL) {
                String messageId = Utility.readFile2String(messageIDFilePath);
                LOGGER.log(Level.INFO, "###RefToMessageId used for pulling: " + messageId);
                if (samlToken != null) {
                    LOGGER.log(Level.INFO, "TaxationClient setting SAML " + LocalDateTime.now());
                    requestMessage.getUserMessage().setSAMLToken(samlToken.toString());
                } else {
                    LOGGER.log(Level.INFO, "Skipping request SAML Token as either the secureMessaging flag is set to false or the retrieved SAML token is null.");
                }

                requestMessage.getSignalMessage().getPullRequest().setRefToMessageId(messageId);
                requestMessage.setResendSpecification(Utility.toIntArray(taxationClientPropsLoader.props.getProperty("Resend-Interval")),
                        taxationClientPropsLoader.props.getProperty("Resend-Interval-Units"));
            }
            // Sign taxationService.message with private key
            if (privateKey != null && certChain != null) {
                LOGGER.log(Level.INFO, "###Signing request message with a certificate " + LocalDateTime.now());
                requestMessage.setSigningCertificate(new KeyStore.PrivateKeyEntry(privateKey, certChain));
            } else {
                LOGGER.log(Level.INFO, "Skipping signing certificate since none specified.");
            }

            String requestSendDateTime = LocalDateTime.now().toString();

            // Sending the request
            LOGGER.log(Level.INFO, "TaxationClient sending request " + LocalDateTime.now());
            Response responseMessage = requestMessage.send();

            String responseDateTime = LocalDateTime.now().toString();

            LOGGER.log(Level.INFO, "TaxationClient received response at: " + LocalDateTime.now());

            // Reacting based on interaction type
            if (interactionType == InteractionType.TWO_WAY_SYNC
                    || interactionType == InteractionType.ONE_WAY_SELECT_PULL) {
                LOGGER.log(Level.INFO, "Save response started " + LocalDateTime.now());
                LOGGER.log(Level.INFO, "Saving response at: " + responseDirectory);
//                saveResponseAttachmentAndPayload(msgPayload, msgAttachments, responseMessage, responseDirectory, attachmentType);
            } else if (interactionType == InteractionType.ONE_WAY_PUSH) {
                Utility.writeString2File(requestMessage.getUserMessage().getMessageId(), messageIDFilePath);
            } else if (interactionType == InteractionType.TWO_WAY_PUSH_PULL) {
                Request followUpRequest = responseMessage.followUp();
                LOGGER.log(Level.INFO, "Retrieving response for two-way-push-pull pattern");
                if (followUpRequest != null) {
                    followUpRequest.setResendSpecification(
                            Utility.toIntArray(taxationClientPropsLoader.props.getProperty("Resend-Interval")),
                            taxationClientPropsLoader.props.getProperty("Resend-Interval-Units"));

                    Response responseFollowUpRequest = followUpRequest.send();
                    saveResponseAttachmentAndPayload(msgPayload, msgAttachments, responseFollowUpRequest, responseDirectory, attachmentType);
                }
            }
            LOGGER.log(Level.INFO, "### in taxationService.TaxationClient save response completed  " + LocalDateTime.now());
            LOGGER.log(Level.INFO, "#### Response Received from MEIG ####");

            if (interactionType == InteractionType.TWO_WAY_SYNC || interactionType == InteractionType.ONE_WAY_PUSH) {
                LOGGER.log(Level.INFO, "Transaction ID: " + requestMessage.getUserMessage().getTransactionId());
                LOGGER.log(Level.INFO, "Message ID: " + requestMessage.getUserMessage().getMessageId());
            }
            if (logInPerformance) {
                if (interactionType != InteractionType.ONE_WAY_SELECT_PULL) {
                    performanceLogString.add(requestMessage
                            .getUserMessage().getMessageId());
                } else {
                    performanceLogString.add(requestMessage.getSignalMessage().getMessageId());
                }
                performanceLogString.add(",");
                performanceLogString.add(refClientStartDate);
                performanceLogString.add(",");
                performanceLogString.add(requestSendDateTime);
                performanceLogString.add(",");
                performanceLogString.add(responseDateTime);
                performanceLogString.add(",");
                performanceLogString.add(ausKeyStart);
                performanceLogString.add(",");
                performanceLogString.add(samlend);
                // Need to adjust the path
                Utility.writeString2File(performanceLogString.toString(), requestBasePath + File.separator + "taxationService.TaxationClient.txt");
            }
        } catch (AS4ClientEbMSException e) {
            LOGGER.log(Level.SEVERE, "Message Send Failed." + e.toString());
            e.printStackTrace();
        } catch (AS4ClientException e) {
            LOGGER.log(Level.SEVERE, "Message Send Failed.");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "A file required to execute AS4Client was not found.");
            e.getMessage();
            e.getStackTrace();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "I/O exception: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.SEVERE, "Illegal Argument Exception: " + e.getMessage());
        } catch (NullPointerException e) {
            LOGGER.log(Level.SEVERE, "Can't find the file");
            e.printStackTrace();
            e.getMessage();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (requestMessage.getUserMessage() != null)
                System.out.println("EBMS3 Request UserMessage Message ID: " + requestMessage.getUserMessage().getMessageId());
            if (requestMessage.getSignalMessage() != null)
                System.out.println("EBMS3 Request SignalMessage Message ID: " + requestMessage.getSignalMessage().getMessageId());
//            msgPayload.closeFileStream();
            if (msgAttachments != null) {
                msgAttachments.closeFileStreams();
            }
        }
    }

    private static void saveResponseAttachmentAndPayload(
            MessagePayload msgPayload, MessageAttachments msgAttachments,
            Response responseMessage, String responsePath, AttachmentType attachmentType) throws IOException {

        // To be optimized
        final Logger LOGGER = Logger.getLogger(TaxationClient.class.getName());
        File dir = new File(responsePath);
        dir.mkdir();

        String responseBasePath;
        String sdateFolderName = LocalDateTime.now().toString();
        responseBasePath = responsePath + File.separator + sdateFolderName;

        dir = new File(responseBasePath);
        dir.mkdir();
        dir = new File(responseBasePath + File.separator + PAYLOAD_BASE_PATH);
        dir.mkdir();
        dir = new File(responseBasePath + File.separator + ATTACHMENTS_BASE_PATH);
        dir.mkdir();
        LOGGER.log(Level.INFO, "### in ReferenceClient writing Response Message " + LocalDateTime.now());

        if (responseMessage != null) {
            /**
             * msgPayload.savePayload(responseMessage,
             * timestampedResponseBasePath + File.separator +
             * PAYLOAD_BASE_PATH);
             **/
            msgAttachments.saveParts(responseMessage, responseBasePath + File.separator + ATTACHMENTS_BASE_PATH, attachmentType);
        } else {
            LOGGER.log(Level.INFO, "###################### in ReferenceClient Response Message NULL  " + LocalDateTime.now());
        }

    }

    private static void createAUSKeyCertificate() {
    }

}

