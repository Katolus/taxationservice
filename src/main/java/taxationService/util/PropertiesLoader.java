package taxationService.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertiesLoader {
    public final Properties props;
    private final Logger LOGGER = Logger.getLogger(PropertiesLoader.class
            .getName());

//    public PropertiesLoader(String propsPath) {
//        super();
//        props = new Properties();
//        LOGGER.log(Level.INFO, "Loading properties file from a path location: " + propsPath);
//        try(FileInputStream inputStream = new FileInputStream(propsPath)) {
//            props.load(inputStream);
//        }catch (IOException e) {
//            LOGGER.log(Level.SEVERE, "Unable to load properties file: " + propsPath);
//            e.printStackTrace();
//        }
//    }

    public PropertiesLoader(String propsPath) {
        super();
        props = new Properties();
        LOGGER.log(Level.INFO, "Loading properties file: " + propsPath);
        try (InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream(propsPath)) {
            props.load(inputStream);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to load properties file: " + propsPath);
            e.printStackTrace();
        }
    }

    public PropertiesLoader(Path propsPath) {
        super();
        props = new Properties();
        LOGGER.log(Level.INFO, "Loading properties file: " + propsPath);
        try (InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream(propsPath.toString())) {
            props.load(inputStream);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to load properties file: " + propsPath);
            e.printStackTrace();
        }
    }

    public List<String> getPropertiesList(List<String> excludedProperties) {

        List<String> propertiesKeys = new ArrayList<>();
        Set<String> keyValues = props.stringPropertyNames();
        if (excludedProperties != null) {
            for (String keyValue : keyValues) {
                if (!excludedProperties.contains(keyValue)) {
                    propertiesKeys.add(keyValue);
                }
            }
        } else {
            propertiesKeys.addAll(keyValues);
        }
        return propertiesKeys;
    }

    public boolean propertiesExistAndNotEmpty() {
        return !this.props.stringPropertyNames().isEmpty();
    }

    public void printPropertiesContent() {
        this.props.stringPropertyNames().stream().forEach(property -> System.out.println(property + "=" + props.getProperty(property)));
    }
}
