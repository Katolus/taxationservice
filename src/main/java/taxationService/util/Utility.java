package taxationService.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import taxationService.TaxationClient;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

// Not sure why all the methods are static
public class Utility {

    private static final Logger LOGGER = Logger.getLogger(Utility.class
            .getName());

    public static void quietClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getParentDirectory(Class mainClass) {
        String relativePath = mainClass.getProtectionDomain().getCodeSource().getLocation().getPath();
        String path = decodeURL(relativePath);
        return path;
    }

    public static String decodeURL(String path) {
        String returnPath = null;
        try {
            returnPath = URLDecoder.decode(path, "utf-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.log(Level.SEVERE, "Unable to decode the path");
            e.printStackTrace();
        }
        return returnPath;
    }

    public static String encodeURL(String path) {
        String returnPath = null;
        try {
            returnPath = URLEncoder.encode(path, "utf-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.log(Level.SEVERE, "Unable to encode the path");
            e.printStackTrace();
        }
        return returnPath;
    }


    public static String stripExtension(String str) {
        // Handle null case specially.

        if (str == null)
            return null;

        // Get position of last '.'.

        int pos = str.lastIndexOf(".");

        // If there wasn't any '.' just return the string as is.

        if (pos == -1)
            return str;

        // Otherwise return the string, up to the dot.

        return str.substring(0, pos);
    }

    public static String encodeFileToBase64Binary(File inputFile) {

        byte[] bytes = loadFile(inputFile);
        String encodedString = Base64.encodeBase64String(bytes);
        return encodedString;
    }

    private static byte[] loadFile(File file) {
        byte[] bytes = null;
        try (InputStream is = new FileInputStream(file)) {

            long length = file.length();
            if (length > Integer.MAX_VALUE) {
                throw new IOException("Could not completely read file -> File is too large"
                        + file.getName());
            }

            // I don't like this casting, possibly refactor
            bytes = new byte[(int) length];

            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }

            if (offset < bytes.length) {
                throw new IOException("Could not completely read file "
                        + file.getName());
            }

        } catch (IOException e) {
            e.getMessage();
            e.printStackTrace();
        }
        return bytes;
    }

    public static String reuseSAMLToken(String path, int tokenValidDuration) throws FileNotFoundException {
        String samlToken = null;

        File fnew = new File(path);
        if (fnew.exists()) {
            long createTime = fnew.lastModified();
            long currentTime = new Date().getTime();
            if (createTime != 0 && (currentTime - createTime) > tokenValidDuration) {
                try {
                    samlToken = readFile2String(path);
                    System.out.println("Out print the samlToken read from the file -> " + samlToken);
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Couldn't find the file from which to read the SAMLToken");
                    e.printStackTrace();
                }
            }
        }else {
            throw new FileNotFoundException("Couldn't find the file through Files.exists(Paths.get(path)), therefore unable to perform read");
        }

        return samlToken;
    }

    public static String readFile2String(String path) throws IOException {
        if (doesAFileUnderAPathExist(path)) {
            String returnString = null;
            try {
                File fnew = new File(path);
                FileInputStream fis = new FileInputStream(fnew);
                byte[] data = new byte[Math.toIntExact(fnew.length())];
                fis.read(data);
                fis.close();
                returnString = new String(data, "UTF-8");
            } catch (ArithmeticException e) {
                LOGGER.log(Level.SEVERE, "Byte conversion constructor caused value overflows an int.");
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                LOGGER.log(Level.SEVERE, "There is an error creating String object from the file");
                e.printStackTrace();
            }
            return returnString;
        } else {
            throw new FileNotFoundException("Couldn't find the file through Files.exists(Paths.get(path)), therefore unable to perform read");
        }
    }

    public static void writeString2File(String newString, String path) throws FileNotFoundException {
        if (doesAFileUnderAPathExist(path)) {
            try (FileWriter fileWriter = new FileWriter(new File(path), false)) {
                fileWriter.write(newString);
                fileWriter.close();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "An error while writing to a file occurred. File should be located under this path -> " + path);
                e.printStackTrace();
            }
        } else {
            throw new FileNotFoundException("Couldn't find the file through Files.exists(Paths.get(path)), therefore unable to perform write");
        }
    }

//    public static void writeString2FileWithResource(String newString, String path) throws FileNotFoundException {
//        if (TaxationClient.class.getClassLoader().getResource(path) != null) {
//            LOGGER.log(Level.INFO, "Writing to a file -> " + path);
//            try (FileWriter fileWriter = new FileWriter(new File(path), false)) {
//                fileWriter.write(newString);
//                fileWriter.close();
//            } catch (IOException e) {
//                LOGGER.log(Level.SEVERE, "An error while writing to a file occurred. File should be located under this path -> " + path);
//                e.printStackTrace();
//            }
//        } else {
//            LOGGER.log(Level.INFO, "There is not file under this location -> " + path);
//            throw new FileNotFoundException("Couldn't find the file through Files.exists(Paths.get(path)), therefore unable to perform write");
//
//        }
//    }

    public static String readFirstLineFromFile(String inputFileLocation) {
        String sCurrentLine = null;
        try (BufferedReader br = new BufferedReader(new FileReader(inputFileLocation))) {
            sCurrentLine = br.readLine();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to read the first line of the file located at: " + inputFileLocation);
            e.printStackTrace();
        }
        if (sCurrentLine == null)
            sCurrentLine = "<!--Unable to locate the comment file or it is empty-->";
        return sCurrentLine;
    }

    public static int[] toIntArray(String intString) {
        String pieces[] = intString.split(",");
        int ints[] = new int[pieces.length];
        for (int i = 0; i < pieces.length; i++) {
            ints[i] = Integer.parseInt(pieces[i]);
        }

        return ints;
    }

    public static boolean doesAFileUnderAPathExist(String path) {
        if (Files.exists(Paths.get(path))) {
            LOGGER.log(Level.INFO, String.format("A file under %s exists.", path));
            return true;
        } else {
            LOGGER.log(Level.INFO, "There is no file under this location -> " + path);
            return false;
        }
    }

    // Possibly to be implemented
    public static boolean doesAFileExistAsAResource(String filePath) {
        if (getDefaultClassLoader().getResource(filePath) !=null) {
            LOGGER.log(Level.INFO, String.format("A file under %s exists.", filePath));
            return true;
        } else {
            LOGGER.log(Level.INFO, "There is no file under this location -> " + filePath);
            return false;
        }
    }


    public static InputStream getResourceAsInputStream(String resourcePattern) {
        if(doesAFileExistAsAResource(resourcePattern)){
            return getDefaultClassLoader().getResourceAsStream(resourcePattern);
        }else{
            LOGGER.log(Level.INFO, "Returned input stream is null");
            return null;
        }
    }

    public static String getInputStreamAsString(InputStream fileStream) throws IOException {
        return IOUtils.toString(fileStream, "UTF-8");
    }

    private static ClassLoader getDefaultClassLoader(){
        return TaxationClient.class.getClassLoader();
    }
}
