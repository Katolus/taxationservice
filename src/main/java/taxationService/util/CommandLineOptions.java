package taxationService.util;

import java.util.HashMap;
import java.util.Map;

public class CommandLineOptions {

    // Locally declared enum class
    public enum Option {
        EXCHANGE_PROFILE("-exchangeProfile", true),
        REQUEST_FOLDER("-requestFolder", true),
        RESPONSE_FOLDER("-responseFolder",false),
        CERTIFICATE_ALIAS("-certificateAlias",true),
        AUSKEY_PASSPHRASE("-ausKeyPassPhrase", true),
        SAML_FLAG("-samlFlag", true),
        VANGUARD_ENDPOINT("-vanguardEndpoint", true),
        OVERRIDE_OWNER_ORGANIZATION_NAME("-overrideOwnerOrgName",false),
        OVERRIDE_OWNER_ORGANIZATION_ROLE("-overrideOwnerOrgRole", false),
        OVERRIDE_OWNER_ORGANIZATION_BUSINESS_ID("-overrideOwnerOrgBusinessId", false),
        OVERRIDE_OWNER_ORGANIZATION_BUSINESS_ID_TYPE("-overrideOwnerOrgBusinessIdType", false),
        ATTACHMENT_TYPE("-attachmentType",false),
        ENABLE_AUDIT("-enableAudit",false);

        private String optionFlag;
        private boolean required;

        Option(String optionFlag, boolean required) {
            this.optionFlag = optionFlag;
            this.required = required;
        }
    }

    public static String getOption(Option option, Map<Option, String> options) {
        String value = options.get(option);
        if (option.required && value == null) {
            printUsage();
            throw new IllegalArgumentException("Required option "
                    + option.optionFlag + " not found.");
        }
        return value;
    }
    public static void printUsage() {
        StringBuilder sb = new StringBuilder();
        sb.append(System.lineSeparator() + "usage: SampleClientUsage");
        for (Option option : Option.values()) {
            sb.append((option.required ? " " : " [") + option.optionFlag + " <"
                    + option.optionFlag.substring(1) + ">"
                    + (option.required ? "" : "]"));
        }
        sb.append(System.lineSeparator());
        System.out.println(sb.toString());
    }

    public static Map<Option, String> getOptions(String[] args) {
        Map<Option, String> options = new HashMap<Option, String>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                if (i < args.length - 1) {
                    boolean foundIt = false;
                    for (Option option : Option.values()) {
                        if (option.optionFlag.equals(args[i])) {
                            foundIt = true;
                            options.put(option, args[++i]);
                            break;
                        }
                    }
                    if (!foundIt) {
                        printUsage();
                        throw new IllegalArgumentException("Command line flag "
                                + args[i] + " not recognized.");
                    }
                } else {
                    printUsage();
                    throw new IllegalArgumentException("Command line flag "
                            + args[i] + " has no option.");
                }
            } else {
                printUsage();
                throw new IllegalArgumentException("Command line option "
                        + args[i] + " has no flag.");
            }
        }
        for (Option option : Option.values()) {
            if (option.required && !options.containsKey(option)) {
                throw new IllegalArgumentException("Required option "
                        + option.optionFlag + " not found.");
            }
        }
        return options;
    }



}
