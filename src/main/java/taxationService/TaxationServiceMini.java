package taxationService;

import com.ibm.b2b.as4.client.*;
import taxationService.security.SecurityCheck;
import taxationService.util.PropertiesLoader;
import taxationService.util.Utility;

import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaxationServiceMini {

    static final Logger LOGGER = Logger.getLogger(TaxationServiceMini.class.getName());

    private static String mainKeyStorePassPhrase = "IGt4a_?6Yui77qwss";
    public final static PropertiesLoader taxationClientPropsLoader = new PropertiesLoader("config/Taxation.properties");


    public static void main(String[] args) {

        final String configDirectory = "config";
        final String dataDirectory = "data";
        final String auditDirectory = "audit";
        final String aType = "SRP";
        final String enableAudit = "true";
        final String profileName = "SampleSignedOneWayPull";
        final String certificateAlias = "ABRP:27809366375_10000022";

        try (AS4Client as4Client = AS4ClientFactory.getClient(configDirectory, dataDirectory, mainKeyStorePassPhrase,
                auditDirectory)) {

            Profile profile = as4Client.getProfile(profileName);
            Request request = as4Client.createRequest(profile);
            request.setResendSpecification(Utility.toIntArray(taxationClientPropsLoader.props.getProperty("Resend-Interval")),
                    taxationClientPropsLoader.props.getProperty("Resend-Interval-Units"));
            RequestSignalMessage requestSignalMessage = request.getSignalMessage();
            SecurityCheck securityCheck = new SecurityCheck(certificateAlias);
            PrivateKey privateKey = securityCheck.getPrivateKey();
            Certificate[] certificates = securityCheck.getCertificateChain();
            requestSignalMessage.setSAMLToken(securityCheck.getSecurityTokenAsString());
            requestSignalMessage.getPullRequest().setRefToMessageId(UUID.randomUUID().toString());
            request.setSigningCertificate(new KeyStore.PrivateKeyEntry(privateKey, certificates));

            LOGGER.log(Level.INFO, "Sending request...");
            Response response = request.send();
//            request.getSignalMessage();
            LOGGER.log(Level.INFO, "Response received.");

        } catch (AS4ClientIllegalRequestException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4ClientIllegalRequestException has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (AS4ClientException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4Client has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "An IOException occurred while creating signal message.");
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An unknown exception has occurred.");
            e.printStackTrace();
        }
    }
}
