package taxationService;

import com.ibm.b2b.as4.client.*;
import taxationService.message.RequestFactory;
import taxationService.util.PropertiesLoader;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaxationSignalMessageClient {

    static final Logger LOGGER = Logger.getLogger(TaxationSignalMessageClient.class.getName());

    public final static PropertiesLoader taxationClientPropsLoader = new PropertiesLoader("config/Taxation.properties");


    public static void main(String[] args) {

        LOGGER.log(Level.INFO, "Starting TaxationClient " + LocalDateTime.now());
        String mainKeyStorePassPhrase = "IGt4a_?6Yui77qwss";
        System.getProperties().setProperty("referenceClient.logPerformanceStats", "true");
        boolean logInPerformance = System.getProperties().getProperty("referenceClient.logPerformanceStats") != null;
        LOGGER.log(Level.INFO, "Program will keep performance logs -> " + logInPerformance);

        final String configDirectory = "config";
        final String dataDirectory = "data";
        final String auditDirectory = "audit";
        final String aType = "SRP";
        final String enableAudit = "true";
        final String profileName = "BatchBulkAsyncPull-EVTE";
        final String certificateAlias = "ABRP:27809366375_10000022";
        final String vanguardServiceType = "test";
        final String overrideOwnerOrgName = "Katoliks";
        final String overrideOwnerOrgRole = "http://sbr.gov.au/ato/Role/Registered Agent";
        final String overrideOwnerOrgBusinessId = "17801003";
        final String overrideOwnerOrgBusinessIdType = "http://ato.gov.au/PartyIdType/TAN";
        final String requestBasePath = "requests";
        final String attachmentsDirectory = "attachments";
        final String messageIDFilePath = "config/async_ids";

        final String pullAction = "pullAction";
        final String pullAgreementRef = "pullAgreementRef";
        final String pullAgreementRefType = "pullAgreementRefType";
        final String pullConversationId = UUID.randomUUID().toString();
        final String pullRefToMessageId = "pullRefToMessageId";
        final String pullServiceName = "pullServiceName";
        final String pullServiceType = "pullServiceType";

        final String destinationURL = "https://test.ato.sbr.gov.au:443/services";

        try (AS4Client as4Client = AS4ClientFactory.getClient(configDirectory, dataDirectory, mainKeyStorePassPhrase,
                auditDirectory)) {

            RequestFactory requestFactory = new RequestFactory(profileName, as4Client);
            requestFactory.createRequest(certificateAlias);
            RequestSignalMessage signalMessage = requestFactory.createSignalMessage(pullAction, pullAgreementRef, pullAgreementRefType,
                    pullConversationId, pullRefToMessageId, pullServiceName, pullServiceType);

            System.out.println(signalMessage.getMessageId());
            System.out.println(signalMessage.getTransactionId());
            Request request = requestFactory.getRequest();

            LOGGER.log(Level.INFO, "Sending request...");
            Response response = request.send();
            LOGGER.log(Level.INFO, "Response received.");

        } catch (AS4ClientIllegalRequestException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4ClientIllegalRequestException has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (AS4ClientException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4Client has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "An IOException occurred while creating signal message.");
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An unknown exception has occurred.");
            e.printStackTrace();
        }

//            DummyPullProfile exchangeProfile = new DummyPullProfile();
//            Profile profile = as4Client.createProfile(exchangeProfile.getExchangeProfile());
//            requestFactory.setProfile(profile);
    }


}
