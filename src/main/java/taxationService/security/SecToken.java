package taxationService.security;

import au.gov.abr.securitytokenmanager.SecurityToken;
import au.gov.abr.securitytokenmanager.SecurityTokenManagerClient;
import au.gov.abr.securitytokenmanager.exceptions.STMException;
import au.gov.abr.securitytokenmanager.exceptions.STMFaultException;

import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class used to obtain a token from the STS.
 * Piotr -> Class build on top of the SecurityToken to mocking data, might be redundant. Consider extending that class.
 */

public class SecToken {

    final static Logger LOGGER = Logger.getLogger(VanGuardServiceType.class.getName());

    public SecToken() {
    }

    /**
     * Get a taxationService.security token from Vanguard to use for secure authentication and
     * communication with the relevant core web service.
     *
     * @param endpointUrl
     * @param stsEndpoint
     * @param certificateChain
     * @param privateKey
     * @return SecurityToken
     */

    public SecurityToken getSecurityToken(String endpointUrl,
                                          VanGuardServiceType vServiceType,
                                          Certificate[] certificateChain,
                                          PrivateKey privateKey) {
        if (vServiceType.getType().equals("MOCK")) {
            // TO DO: this is not looking too optimal (to same in System properties)
            // Depedent on the ABR_SecurityTokenManager implementation as they use System.getProperties
            System.setProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties", "src/main/resources/config/sts_mock.properties");
        } else {
            System.setProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties", "src/main/resources/config/sts.properties");
        }
        System.setProperty("au.gov.abr.securitytokenmanager.properties", "src/main/resources/config/securitytokenmanager.properties");
        SecurityTokenManagerClient stmClient = new SecurityTokenManagerClient();
        // use the STM getSecurityIdentityToken method
        // to get a proof token and assertion for invoking a
        // SBR Core Services 2011 Web service
        SecurityToken response = null;
        try {
            response = stmClient.getSecurityIdentityToken(endpointUrl, null, privateKey, certificateChain);
        } catch (STMFaultException e) {
            LOGGER.log(Level.SEVERE, e.getReasonText());
            e.printStackTrace();
        } catch (STMException e) {
            LOGGER.log(Level.SEVERE, "An exception other than STMFaultException has occurred");
            e.printStackTrace();
        }
        return response;
    }

}
