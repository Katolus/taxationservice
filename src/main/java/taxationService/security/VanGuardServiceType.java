package taxationService.security;

import java.util.logging.Level;
import java.util.logging.Logger;

public enum VanGuardServiceType {

    MOCK_Endpoint("https://dev.authentication.business.gov.au:5007","MOCK"),
    TEST_Endpoint("https://thirdparty.authentication.business.gov.au/R3.0/vanguard/S007v1.2/service.svc","TEST");

    private String vanGuardEndpoint;
    private String type;

    private final static Logger LOGGER = Logger.getLogger(VanGuardServiceType.class.getName());

    VanGuardServiceType(String endpoint, String type){
        this.vanGuardEndpoint = endpoint;
        this.type = type;
    }

//    public String getVanGuardEndpoint(){
//        return this.vanGuardEndpoint;
//    }

    public String getType(){
        return this.type;
    }

    public static VanGuardServiceType fromString(String textType){
        if (textType != null) {
            for (VanGuardServiceType vanValue : VanGuardServiceType.values()) {
                if (textType.equalsIgnoreCase(vanValue.type)) {
                    LOGGER.log(Level.INFO, String.format("VanGuardService type %s. Calling %s", vanValue.type, vanValue.vanGuardEndpoint));
                    return vanValue;
                }
            }
        }
        LOGGER.log(Level.INFO, "Unable to find any matching value in enum VanguardServiceType!!");
        return null;
    }
}
