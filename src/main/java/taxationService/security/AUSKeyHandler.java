package taxationService.security;

import au.gov.abr.akm.credential.store.ABRCredential;
import au.gov.abr.akm.credential.store.ABRKeyStore;
import au.gov.abr.akm.credential.store.ABRProperties;
import au.gov.abr.akm.exceptions.*;
import au.gov.abr.securitytokenmanager.SecurityToken;
import au.gov.abr.securitytokenmanager.SecurityTokenManagerClient;
import au.gov.abr.securitytokenmanager.exceptions.STMException;
import au.gov.abr.securitytokenmanager.exceptions.STMFaultException;
import taxationService.util.Utility;

import java.io.*;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import static taxationService.TaxationSignalMessageClient.taxationClientPropsLoader;
import static taxationService.util.Utility.*;

public class AUSKeyHandler {

    private String keyStorePassword;
    private ABRKeyStore keyStore = null;
    private static final String DATE = "April 2018";
    private static final String VERSION_NUMBER = "v1.0";
    private static final String SOFTWARE_NAME = "EBMS3 Taxation Client";
    private static final String ORG_NAME = "ATO";

    final static Logger LOGGER = Logger.getLogger(AUSKeyHandler.class.getName());

    public AUSKeyHandler(String keyStorePassword, String keyStoreFile) {
        super();
        this.keyStorePassword = keyStorePassword;
        try (InputStream inputStream = getResourceAsInputStream(keyStoreFile)) {
            LOGGER.log(Level.INFO, "Loading keyStore file: " + keyStoreFile);
            ABRProperties.setSoftwareInfo(ORG_NAME, SOFTWARE_NAME, VERSION_NUMBER, DATE);
            this.keyStore = new ABRKeyStore(inputStream);
        } catch (SDKExpiredException e) {
            LOGGER.log(Level.SEVERE, "There was an error handling AUSKey, check if your SDK version have not expired");
            e.printStackTrace();
        } catch (KeyStoreLoadException e) {
            LOGGER.log(Level.SEVERE, "An error loading keyStore");
            e.printStackTrace();
        } catch (NullReferenceException e) {
            LOGGER.log(Level.SEVERE, "There was an error handling AUSKey, no keyStore found");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "There was an error handling AUSKey, no file to load the stream from");
            e.printStackTrace();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "There was an error handling AUSKey");
            e.printStackTrace();
        }
    }

    public PrivateKey getPrivateKey(String certificateAlias) {
        PrivateKey privateKey = null;
        try {
            ABRCredential credential = this.keyStore.getCredential(certificateAlias);
            if (credential.isReadyForRenewal()) {
                // credential.renew(keyPassword.toCharArray());
                LOGGER.log(Level.INFO, "Credential is ready for renewal");
            }
            privateKey = credential.getPrivateKey(keyStorePassword.toCharArray());
        } catch (NoSuchAliasException e) {
            LOGGER.log(Level.SEVERE, "Error loading the certificate with the specified certificate alias");
            e.printStackTrace();
        } catch (IncorrectPasswordException e) {
            LOGGER.log(Level.SEVERE, "Invalid password for the AUSKey keystore");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public X509Certificate[] getCertificateChain(String certificateAlias) {
        X509Certificate[] certificateChain = null;
        try {
            ABRCredential credential = keyStore.getCredential(certificateAlias);
            certificateChain = credential.getCertificateChain();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error loading the certificate with specified alias from the AUSKey Keystore");
            e.printStackTrace();
        }
        return certificateChain;
    }

    public SecurityToken getSAMLToken(String certificateAlias, Certificate[] certificateChain, PrivateKey privateKey,
                                      String vanguardServiceType, String serviceEndpoint) {

        String samlLocationPath = taxationClientPropsLoader.props.getProperty("SAMLSavePath", "config/savedSAML") + "/" + certificateAlias.substring(5) + ".saml";

        VanGuardServiceType vanGuardServiceType = VanGuardServiceType.fromString(vanguardServiceType);
        if (vanGuardServiceType.getType().equals("MOCK")) {
            // TO DO: this is not looking too optimal (to same in System properties)
            // Dependent on the ABR_SecurityTokenManager implementation as they use System.getProperties
            System.setProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties", "src/main/resources/config/sts_mock.properties");
        } else {
            System.setProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties", "src/main/resources/config/sts.properties");
        }
        System.setProperty("au.gov.abr.securitytokenmanager.properties", "src/main/resources/config/securitytokenmanager.properties");

        SecurityTokenManagerClient stmClient = new SecurityTokenManagerClient();
        SecurityToken securityToken = null;

//        LOGGER.log(Level.INFO, "Private key ->" + privateKey.toString());
//        LOGGER.log(Level.INFO, "Certificate key ->" + certificateChain[0].toString());
        try {
            securityToken = stmClient.getSecurityIdentityToken(serviceEndpoint, null, privateKey, certificateChain);
        } catch (STMFaultException e) {
            LOGGER.log(Level.SEVERE, e.getReasonText());
            e.printStackTrace();
        } catch (STMException e) {
            LOGGER.log(Level.SEVERE, "An exception other than STMFaultException has occurred");
            e.printStackTrace();
        }

        writeToFileIfNotNull(securityToken, samlLocationPath);
        return securityToken;

    }

    private void writeToFileIfNotNull(SecurityToken securityToken, String samlLocationPath) {
        if (securityToken != null) {
            try {
                FileOutputStream outputStream = new FileOutputStream(samlLocationPath);
                outputStream.write(securityToken.getAssertionAsString().getBytes());
                LOGGER.log(Level.INFO, "Writing SAML Token to a file located under this path -> " + samlLocationPath);
            } catch (FileNotFoundException e) {
                LOGGER.log(Level.SEVERE, "File under this location not found -> " + samlLocationPath);
                e.printStackTrace();
            } catch (STMException e) {
                LOGGER.log(Level.SEVERE, "Error asserting security token to string");
                e.printStackTrace();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Unable to find write to a file " + samlLocationPath);
                e.printStackTrace();
            }
        } else {
            LOGGER.log(Level.INFO, "Returned security Token is null.");
        }
    }

}
