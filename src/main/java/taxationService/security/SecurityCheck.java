package taxationService.security;

import au.gov.abr.securitytokenmanager.SecurityToken;
import au.gov.abr.securitytokenmanager.exceptions.STMException;
import taxationService.util.Utility;

import java.io.FileNotFoundException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import static taxationService.TaxationSignalMessageClient.taxationClientPropsLoader;
import static taxationService.util.Utility.doesAFileUnderAPathExist;

public class SecurityCheck {

    private final String certificateAlias;
    private final String samlLocationPath;
    private Integer samlExpireTime;
    private AUSKeyHandler ausKeyHandler = null;
    private PrivateKey privateKey = null;
    private Certificate[] certificateChain = null;
    private SecurityToken securityToken = null;

    final static Logger LOGGER = Logger.getLogger(SecurityCheck.class.getName());

    public SecurityCheck(String certificateAlias) {
        this.certificateAlias = certificateAlias;
        samlLocationPath = taxationClientPropsLoader.props.getProperty("SAMLSavePath", "config/savedSAML") + "/" + certificateAlias.substring(5) + ".saml";
        samlExpireTime = Integer.valueOf(taxationClientPropsLoader.props.getProperty("Saml-Valid-For", "180000"));

        String keyStorePath = taxationClientPropsLoader.props.getProperty("AusKeystore-Location");

        if (keyStorePath != null && Utility.doesAFileExistAsAResource(keyStorePath)) {
            String keystorePassphrase = taxationClientPropsLoader.props.getProperty("KeystorePassphrase", "Password1!");
            ausKeyHandler = new AUSKeyHandler(keystorePassphrase, keyStorePath);
            privateKey = ausKeyHandler.getPrivateKey(certificateAlias);
            certificateChain = ausKeyHandler.getCertificateChain(certificateAlias);

        } else {
            LOGGER.log(Level.INFO, "Skipping key and certificate chain lookup since no key store specified.");
        }
    }

    public String getSecurityTokenAsString() throws STMException, FileNotFoundException {

        if (checkIfRelatedSAMLAlreadySaved(samlLocationPath)) {
            LOGGER.log(Level.INFO,"Reusing previously generated SAML Token -> "+samlLocationPath);
            return Utility.reuseSAMLToken(samlLocationPath, samlExpireTime);
        } else {
            LOGGER.log(Level.INFO, "Performing a call to fetch SAML Token");
            String serviceEndPoint = taxationClientPropsLoader.props.getProperty("Service_Endpoint", "https://test.sbr.gov.au/services");
            String vanguardServiceType = taxationClientPropsLoader.props.getProperty("VanguardServiceType", "MOCK");
            securityToken = ausKeyHandler.getSAMLToken(certificateAlias, certificateChain, privateKey, vanguardServiceType, serviceEndPoint);
            if (this.securityToken != null) {
//                System.out.println(this.securityToken.getAssertionAsString());
                return this.securityToken.getAssertionAsString();
            } else {
                throw new IllegalArgumentException("Security Token is null");
            }
        }
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public Certificate[] getCertificateChain() {
        return certificateChain;
    }

    private static boolean checkIfRelatedSAMLAlreadySaved(String samlLocationPath) {
        return doesAFileUnderAPathExist(samlLocationPath);
    }
}
