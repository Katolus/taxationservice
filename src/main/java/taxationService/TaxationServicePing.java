package taxationService;

import com.ibm.b2b.as4.client.*;
import taxationService.message.RequestFactory;
import taxationService.security.SecurityCheck;
import taxationService.util.PropertiesLoader;
import taxationService.util.Utility;

import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaxationServicePing {

    static final Logger LOGGER = Logger.getLogger(TaxationServicePing.class.getName());
    public final static PropertiesLoader taxationClientPropsLoader = new PropertiesLoader("config/Taxation.properties");

    public static void main(String[] args) {
        final String mainKeyStorePassPhrase = "IGt4a_?6Yui77qwss";
        final String configDirectory = taxationClientPropsLoader.props.getProperty("AS4ClientConfigDirectory", "config");
        final String dataDirectory = taxationClientPropsLoader.props.getProperty("AS4ClientDataDirectory", "data");
        final String auditDirectory = taxationClientPropsLoader.props.getProperty("AS4ClientAuditDirectory", "audit");
        final String payloadPath = taxationClientPropsLoader.props.getProperty("PayLoadDirectory", null);
        final String payloadContentType = taxationClientPropsLoader.props.getProperty("PayLoadContentType", "text/xml");
        final String payload = taxationClientPropsLoader.props.getProperty("Payload", null);
        final String amplifyPayload = taxationClientPropsLoader.props.getProperty("AmplifyPayload", null);
        final String attachmentPath = taxationClientPropsLoader.props.getProperty("AttachmentDirectory", null);
        final String service = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/service";
        final String action = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/test";
        final String profileName = "SampleSignedOneWayPush";
        final String certificateAlias = "ABRP:27809366375_10000022";

        try (AS4Client as4Client = AS4ClientFactory.getClient(configDirectory, dataDirectory, mainKeyStorePassPhrase,
                auditDirectory)) {

            Profile profile = as4Client.getProfile(profileName);
            Request request = as4Client.createRequest(profile);


            RequestUserMessage requestUserMessage = request.getUserMessage();
            requestUserMessage.setActionName(action);
            requestUserMessage.setService(service, null);
            requestUserMessage.setConversationId(UUID.randomUUID().toString());


            SecurityCheck securityCheck = new SecurityCheck(certificateAlias);
            PrivateKey privateKey = securityCheck.getPrivateKey();
            Certificate[] certificates = securityCheck.getCertificateChain();
            requestUserMessage.setSAMLToken(securityCheck.getSecurityTokenAsString());
            request.setSigningCertificate(new KeyStore.PrivateKeyEntry(privateKey, certificates));

            LOGGER.log(Level.INFO, "Sending request...");
            Response response = request.send();
            LOGGER.log(Level.INFO, "Response received.");

        } catch (AS4ClientIllegalRequestException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4ClientIllegalRequestException has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (AS4ClientException e) {
            LOGGER.log(Level.SEVERE, "An error instantiating AS4Client has occurred.");
            PropertiesLoader errorCodeProperties = new PropertiesLoader("errorMessageCodes/MessageBundle.properties");
            LOGGER.log(Level.SEVERE, errorCodeProperties.props.getProperty(e.getErrorCode(), "No related code found"));
            e.printStackTrace();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "An IOException occurred while creating signal message.");
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An unknown exception has occurred.");
            e.printStackTrace();
        }
    }
}
