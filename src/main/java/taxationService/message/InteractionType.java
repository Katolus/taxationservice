package taxationService.message;

import com.ibm.b2b.as4.client.Constants;

import java.util.logging.Level;
import java.util.logging.Logger;

public enum InteractionType {

    TWO_WAY_SYNC(Constants.MEP_TWO_WAY_SYNC), ONE_WAY_PUSH(Constants.MEP_ONE_WAY_PUSH), ONE_WAY_SELECT_PULL(
            Constants.MEP_ONE_WAY_PULL),TWO_WAY_PUSH_PULL(Constants.MEP_TWO_WAY_PUSH_PULL);

    final static Logger LOGGER = Logger.getLogger(InteractionType.class.getName());
    private String interactionType;

    InteractionType(String interactionType){
        this.interactionType = interactionType;
    }
    public String getInteractionType(){
        return this.interactionType;
    }
    public static InteractionType fromString(String text){
        if(text != null){
            for(InteractionType type : InteractionType.values()){
                if(text.equalsIgnoreCase(type.interactionType)){
                    LOGGER.log(Level.INFO, String.format("A type of %s has been matched with enum InteractionType", type.interactionType));
                    return type;
                }
            }
        }
        LOGGER.log(Level.INFO, "Unable to find any matching value in enum InteractionType");
        return null;
    }


}
