package taxationService.message;

import com.ibm.b2b.as4.client.*;
import taxationService.util.PropertiesLoader;
import taxationService.util.Utility;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageAttachments {

    private Path attachmentRootFolder;
    private List<FileInputStream> inputStreamList = null;
    private List<ByteArrayInputStream> byteInputStreamList = null;
    private List<FileOutputStream> outputStreamList = null;
    private InteractionType interactionType = null;
    private final String ELS_APPROVAL_FLAG = "ELS Approval Number";
    private final Logger LOGGER = Logger.getLogger(MessageAttachments.class.getName());

    public MessageAttachments(String attachmentRootFolder,
                              InteractionType interactionType) {
        super();
        this.attachmentRootFolder = Paths.get(attachmentRootFolder);
        this.interactionType = interactionType;
        inputStreamList = new ArrayList<>();
        byteInputStreamList = new ArrayList<>();
        LOGGER.log(Level.INFO, "Attachment Root Folder: "
                + attachmentRootFolder);
    }

    public void attachParts(Request requestMessage, AttachmentType atype){
        RequestUserMessage requestUserMessage = requestMessage.getUserMessage();
        if (interactionType == InteractionType.TWO_WAY_SYNC
                || (interactionType == InteractionType.TWO_WAY_PUSH_PULL && (atype == null || atype == AttachmentType.SRP)))
            assembleSRPRequest(requestUserMessage);
        else if (interactionType == InteractionType.ONE_WAY_PUSH
                || (interactionType == InteractionType.TWO_WAY_PUSH_PULL && atype == AttachmentType.BBRP))
            assembleBBRPRequest(requestUserMessage);

    }

    private void assembleSRPRequest(RequestUserMessage requestUserMessage) {
        LOGGER.log(Level.INFO, "Log the attachment folder before toFile() method is called" + this.attachmentRootFolder);
        RequestPart part = requestUserMessage.createPart();

        PropertiesLoader propsLoader;
        String propsPath = attachmentRootFolder + "/config/SRP.properties";
//            String propsPath = attachmentRootFolder + Utility.stripExtension(child.getName()) + ".properties";
        LOGGER.log(Level.INFO, "Loading Attachments properties from: " + propsPath);

        propsLoader = new PropertiesLoader(propsPath);
        try (FileInputStream attachmentFileInputStream = new FileInputStream(this.getClass().getClassLoader().getResource(propsPath).getPath())) {
            propsLoader.printPropertiesContent();
            inputStreamList.add(attachmentFileInputStream);
            part.setContentID(propsLoader.props.getProperty("ContentID"));
            part.setContentType(propsLoader.props.getProperty("ContentType"));
            part.setOriginalFileName(propsLoader.props.getProperty("OriginalFileName"));
            part.setInputStream(attachmentFileInputStream);

            LOGGER.log(Level.INFO, "Request part content from properties file loaded");
            List<String> excludedProperties = Arrays.asList("ContentID", "ContentType", "OriginalFileName");
            List<String> propertyKeys = propsLoader.getPropertiesList(excludedProperties);

            propertyKeys.stream().forEach(key -> part.setProperty(key, propsLoader.props.getProperty(key)));

            requestUserMessage.addAttachmentPart(part);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to create a FileInput Stream under this path: " + propsPath);
            e.printStackTrace();
        }

        LOGGER.log(Level.INFO, "Assemble SRP request method completed");
    }

    private void assembleBBRPRequest(RequestUserMessage requestUserMessage) {
        File rootDir = attachmentRootFolder.toFile();

        RequestPart part = requestUserMessage.createPart();

        LOGGER.log(Level.INFO, "Loading Attachments from: " + rootDir);

        PropertiesLoader attachmentPropsLoader;
        String attachmentPropsPath = attachmentRootFolder + "/config/BBRP.properties";
        LOGGER.log(Level.INFO, "Loading Attachments properties from: "
                + attachmentPropsPath);

        attachmentPropsLoader = new PropertiesLoader(attachmentPropsPath);
        part.setContentID(attachmentPropsLoader.props.getProperty("ContentID"));
        part.setContentType(attachmentPropsLoader.props.getProperty("ContentType"));
        part.setOriginalFileName(attachmentPropsLoader.props.getProperty("OriginalFileName"));
        List<String> excludedProperties = Arrays.asList("ContentID", "ContentType", "OriginalFileName");
        List<String> propertyKeys = attachmentPropsLoader.getPropertiesList(excludedProperties);

        boolean elsFlag = false;
        for (String key : propertyKeys) {
            part.setProperty(key,
                    attachmentPropsLoader.props.getProperty(key));
            if (key.trim().contains(ELS_APPROVAL_FLAG))
                elsFlag = true;
        }

        String bbrpDataPath = attachmentRootFolder + "\\BBRPData\\";
        LOGGER.log(Level.INFO, "BBRPData Base Path: " + bbrpDataPath);
        // In-case of ELS request we don't merge files and there is no need to add record delimiter
        // The ELS requests will have an additional Attachment Property called "ELS Approval Number"

        if (elsFlag == true) {
            File bbrpDataRoot = new File(bbrpDataPath);
            File[] bbrpDataRootFiles = bbrpDataRoot.listFiles(File::isFile);

            if (bbrpDataRootFiles.length > 1) {
                LOGGER.log(Level.INFO, "Found multiple attachment files which is not expected in case of ELS prior year requests. Check your request folder");
            }
            try {
                byte[] data = Files.readAllBytes(bbrpDataRootFiles[0].toPath());
                ByteArrayInputStream bytesData = new ByteArrayInputStream(data);
                byteInputStreamList.add(bytesData);
                part.setInputStream(bytesData);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Problem reading files during a BBRP assemble request");
                e.printStackTrace();
            }
        } else {
            // prev StringBuffer strContent = new StringBuffer("");
            StringJoiner strContent = new StringJoiner("");
            File bbrpDataRoot = new File(bbrpDataPath);
            int counter = 0;
            for (File child : rootDir.listFiles(File::isFile)) {

                strContent.add("<Record_Delimiter ");
                String bbrpPropsPath = attachmentRootFolder + "\\BBRPData\\RecordDelimiter\\"
                        + Utility.stripExtension(child.getName()) + ".props";
                PropertiesLoader bbrpPropsLoader = new PropertiesLoader(bbrpPropsPath);
                List<String> bbrpPropertyKeys = bbrpPropsLoader.getPropertiesList(null);
                boolean binaryAttachment = false;
                int size = bbrpPropertyKeys.size();
                for (int j = 0; j < size; j++) {
                    String propKey = bbrpPropertyKeys.get(j);
                    String propValue = bbrpPropsLoader.props
                            .getProperty(bbrpPropertyKeys.get(j));
                    if (propKey.trim().equals("DocumentType"))
                        if (propValue.trim().equalsIgnoreCase("BINARY"))
                            binaryAttachment = true;
                    strContent.add(propKey);
                    strContent.add("=\"");
                    strContent.add(propValue);
                    strContent.add("\"");
                    if (j != (size - 1)) {
                        strContent.add(" ");
                    } else {
                        strContent.add("/>");
                    }
                }

                int ch;
                if (binaryAttachment != true) {
                    try (FileInputStream inputStream = new FileInputStream(child)) {
                        inputStreamList.add(inputStream);
                        while ((ch = inputStream.read()) != -1) {
                            strContent.add(Integer.toString(ch));
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    // This is a binary attachment
                    strContent.add(Utility
                            .encodeFileToBase64Binary(child));
                }
                strContent.add(System.lineSeparator());
            }

            LOGGER.log(Level.INFO, "Push Request");
            LOGGER.log(Level.INFO, strContent.toString());
            ByteArrayInputStream byteInput = new ByteArrayInputStream(
                    strContent.toString().getBytes(StandardCharsets.UTF_8));
            byteInputStreamList.add(byteInput);
            part.setInputStream(byteInput);
        }
        requestUserMessage.addAttachmentPart(part);

        LOGGER.log(Level.INFO, "Assemble BBRP request method completed");
    }

    public void saveParts(Response response, String basePath,
                          AttachmentType attachmentType) throws IOException {
        ResponsePart[] parts;

        if (response == null) {
            LOGGER.log(Level.INFO, "###TaxationClient-saveParts response null");
        } else {
            try {
                ResponseUserMessage responseMessage = response.getUserMessage();
                if (responseMessage == null) {
                    LOGGER.log(Level.INFO, "###TaxationClient-saveParts responseUserMessage is null");
                } else {

                    parts = responseMessage.getAttachments();

                    if (parts == null) {
                        LOGGER.log(Level.INFO,
                                "###TaxationClient-saveParts parts are null");

                    } else {
                        LOGGER.log(Level.INFO, "###TaxationClient-saveParts parts are not null" + parts.length);
                        outputStreamList = new ArrayList<>();
                        int i = 0;
                        byte[] buffer = new byte[1024];
                        int n;
                        String fileExtension = null;

                        if (this.interactionType == InteractionType.TWO_WAY_SYNC)
                            fileExtension = "xml";
                        if (this.interactionType == InteractionType.ONE_WAY_SELECT_PULL)
                            fileExtension = "txt";
                        if (this.interactionType == InteractionType.TWO_WAY_PUSH_PULL) {
                            if (attachmentType == null || attachmentType == AttachmentType.SRP)
                                fileExtension = "xml";
                            else
                                fileExtension = "txt";
                        }
                        for (ResponsePart part : parts) {
                            // String fileExtension = ".dat";
                            if (part.getProperties() != null) {
                                for (Map.Entry<String, String> pair : part.getProperties().entrySet()) {
                                    System.out.println(pair.getKey() + " = " + pair.getValue());
                                }
                            }
                            if (i > 1 || this.interactionType.getInteractionType().equals("One-Way/Pull")) {
                                String cFileExtension = retrieveExtensionFromMIMEType(part);
                                fileExtension = cFileExtension != null ? cFileExtension : fileExtension;
                            }

                            String responseAttachmentFilename = basePath + File.separator + "response-attachment-"
                                    + ++i + "." + fileExtension;
                            LOGGER.log(Level.INFO, "Writing response attachment to " + responseAttachmentFilename);
                            FileOutputStream responseAttachmentFileStream = new FileOutputStream(responseAttachmentFilename, false);
                            outputStreamList.add(responseAttachmentFileStream);
                            InputStream in = part.getInputStream();
                            while ((n = in.read(buffer)) > 0) {
                                responseAttachmentFileStream.write(buffer, 0, n);
                            }
                            in.close();
                            LOGGER.log(Level.INFO, "Response Attachment Properties: " + part.getProperties());
                        }
                    }
                }
            } catch (AS4ClientException e) {
                LOGGER.log(Level.SEVERE,
                        "Unable to retrieve response attachments");
                e.printStackTrace();
            }
        }
    }

    private String retrieveExtensionFromMIMEType(ResponsePart part) {
        String fileExtension = null;

        if (part.getProperties() != null) {
            for (Map.Entry<String, String> pair : part.getProperties().entrySet()) {
                LOGGER.log(Level.INFO, pair.getKey() + " = " + pair.getValue());
                if (pair.getKey().equalsIgnoreCase("mimeType")) {
                    try {
                        fileExtension = pair.getValue().split("/")[1];
                    } catch (Exception ex) {
                        ex.getStackTrace();
                    }
                }
            }
        }
        LOGGER.log(Level.INFO, "Attachment Mime Type: " + fileExtension);

        if (this.interactionType.equals(InteractionType.ONE_WAY_SELECT_PULL)) {
            if (fileExtension.equals("zip"))
                return fileExtension;
            else
                return null;
        } else if (fileExtension.equals("json"))
            return fileExtension;
        else
            return null;
    }

    public void closeFileStreams() {
        if (inputStreamList != null) {
            for (FileInputStream is : inputStreamList) {
                Utility.quietClose(is);
            }
        }
        if (outputStreamList != null) {
            for (FileOutputStream os : outputStreamList) {
                Utility.quietClose(os);
            }
        }
        if (byteInputStreamList != null) {
            for (ByteArrayInputStream os : byteInputStreamList) {
                Utility.quietClose(os);
            }
        }
    }


}
