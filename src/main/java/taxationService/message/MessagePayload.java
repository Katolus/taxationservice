package taxationService.message;

import com.ibm.b2b.as4.client.*;
import taxationService.util.Utility;

import java.io.*;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessagePayload {

    private String payloadPath = null;
    private InputStream payloadFileInputStream = null;
    private FileOutputStream payloadFileOutputStream = null;
    final static Logger LOGGER = Logger.getLogger(InteractionType.class.getName());

    public MessagePayload(String payloadPath) {
        super();
        this.payloadPath = payloadPath;
    }

    // Add a payload to a user taxationService.message flow
    public void addPayload(Request requestMessage) {
        RequestUserMessage requestUserMessage = requestMessage.getUserMessage();
        RequestPart payloadPart = requestUserMessage.createPart();

        payloadFileInputStream = Utility.getResourceAsInputStream(payloadPath);
        payloadPart.setInputStream(payloadFileInputStream);

        payloadPart.setContentType("text/xml");
        payloadPart.setProperty("PayloadPartProp1", "PayloadPartValue1");
        payloadPart.setProperty("PayloadPartProp2", "PayloadPartValue2");

        requestUserMessage.setPayloadPart(payloadPart);

        LOGGER.log(Level.INFO, "Taxation-addPayload responseMessage" + requestMessage);
    }

    public void savePayload(Response responseMessage, String basePath) {

        LOGGER.log(Level.INFO, "TaxationClient-savePayload responseMessage: " + responseMessage);
        LOGGER.log(Level.INFO, "TaxationClient-savePayload basePath: " + basePath);

        if (responseMessage == null) {
            LOGGER.log(Level.INFO, "TaxationClient-savePayload response null: " + responseMessage);
        } else {
            ResponseUserMessage responseUserMessage = null;

            try {
                responseUserMessage = responseMessage.getUserMessage();
            } catch (AS4ClientException e) {
                LOGGER.log(Level.SEVERE, "TaxationClient-savePayload response " + e.getMessage());
                e.printStackTrace();
            }

            if (responseUserMessage == null) {
                LOGGER.log(Level.INFO, "TaxationClient responseUserMessage is null");
            } else {
                LOGGER.log(Level.INFO, "TaxationClient responseUserMessage is not null");
                ResponsePart responsePayload = null;

                try {
                    LOGGER.log(Level.INFO, "start saving Payload");
                    responsePayload = responseUserMessage.getPayload();

                    if (responsePayload == null) {
                        LOGGER.log(Level.INFO, "axationClient responsePayload is null");
                    } else {
                        LOGGER.log(Level.INFO, "TaxationClient responsePayload is not null");
                        LOGGER.log(Level.INFO, "Response Message Properties: " + responsePayload.getProperties());
                        String responsePayloadFilename = basePath + "\\payload.dat";
                        LOGGER.log(Level.INFO, "Writing response payload to " + responsePayloadFilename);

                        payloadFileOutputStream = new FileOutputStream(responsePayloadFilename, false);

                        byte[] buffer = new byte[1024];
                        int n;
                        while ((n = responsePayload.getInputStream().read(buffer)) > 0) {
                            LOGGER.log(Level.INFO, "writing payload to file");
                            payloadFileOutputStream.write(buffer, 0, n);
                        }
                        LOGGER.log(Level.INFO, "Response Payload Properties: " + responsePayload.getProperties());

                    }
                } catch (AS4ClientException e) {
                    LOGGER.log(Level.SEVERE, "Unable to retrieve response payload");
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    LOGGER.log(Level.SEVERE, "Unable to find the output file");
                    e.printStackTrace();
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Unable to read the input stream of the response taxationService.message");
                    e.printStackTrace();
                }
            }
        }
    }

    public void closeFileStream(){
        Utility.quietClose(payloadFileInputStream);
        Utility.quietClose(payloadFileOutputStream);
    }


}
