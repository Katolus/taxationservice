package taxationService.message;

import au.gov.abr.securitytokenmanager.exceptions.STMException;
import com.ibm.b2b.as4.client.*;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import taxationService.security.SecurityCheck;
import taxationService.util.PropertiesLoader;
import taxationService.util.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import static taxationService.util.Utility.*;

public class RequestFactory {

    private AS4Client as4Client = null;

    private Request request;
    private RequestUserMessage requestUserMessage;
    private SecurityCheck securityCheck = null;
    private Profile profile;
    private String profileName;

    private PropertiesLoader propsLoader;
    private boolean secureMessagingFlag = false;

    private final String REFERENCE_CLIENT_VERSION = "1.0";
    private String requestPropertyFilePath = "requests/request.properties";

    private String overrideOwnerOrgName;
    private String overrideOwnerOrgRole;
    private String overrideOwnerOrgBusinessId;
    private String overrideOwnerOrgBusinessIdType;

    private final Logger LOGGER = Logger.getLogger(RequestFactory.class.getName());

    public RequestFactory(String profileName) {
        this.profileName = profileName;
    }

    public RequestFactory(AS4Client as4Client) {
        propsLoader = new PropertiesLoader(requestPropertyFilePath);
        secureMessagingFlag = Boolean.parseBoolean(new PropertiesLoader("config/Taxation.properties").props.getProperty("SecureMessagingFlag"));
        if (propsLoader.propertiesExistAndNotEmpty()) {
            LOGGER.log(Level.INFO, "Loading request property file from " + requestPropertyFilePath);
        } else {
            LOGGER.log(Level.INFO, "No request property file found under this location " + requestPropertyFilePath + "or the property file is empty ");
        }
        this.as4Client = as4Client;
    }

    public RequestFactory(String profileName, AS4Client as4Client) {
        this(as4Client);
        if (profileName != null) {
            this.profileName = profileName;
            this.getProfileFromXMLTemplete();
        } else {
            LOGGER.log(Level.INFO, "Profile name is null!");
        }
    }

    private void getProfileFromXMLTemplete() {
        if (profileName != null) {
            try {
                Profile profile = this.as4Client.getProfile(profileName);
                if (profile != null) {
                    this.profile = profile;
                } else {
                    throw new NullArgumentException("Profile returned from the AS4Client is null");
                }
            } catch (AS4ClientException e) {
                LOGGER.log(Level.SEVERE, "Unable to to create a profile: " + profileName + "at: " + LocalDateTime.now());
                e.printStackTrace();
            }
        } else {
            LOGGER.log(Level.SEVERE, "Profile name is null, there cannot call getProfile on AS4Client.");
        }

    }

    public void createRequest(String certificateAlias) throws AS4ClientException {
        if (profile != null) {
            LOGGER.log(Level.INFO, "AS4Client profile is not null");
            profile.setOwner(overrideOwnerOrgName, overrideOwnerOrgRole,
                    overrideOwnerOrgBusinessId, overrideOwnerOrgBusinessIdType);

            setProfileTransportRetries();
            Request request = as4Client.createRequest(profile);
            if (secureMessagingFlag) {
                securityCheck = new SecurityCheck(certificateAlias);
                PrivateKey privateKey = securityCheck.getPrivateKey();
                Certificate[] certificates = securityCheck.getCertificateChain();
                if (privateKey != null && certificates != null) {
                    request.setSigningCertificate(new KeyStore.PrivateKeyEntry(privateKey, certificates));
                } else {
                    LOGGER.log(Level.SEVERE, String.format("PrivateKey -> %s or Certificate %s returned an empty value.", privateKey, certificates));
                }
            } else {
                LOGGER.log(Level.INFO, "Security check flag is -> " + secureMessagingFlag);
            }
            this.request = request;
            LOGGER.log(Level.INFO, "Request is created an it is !=null -> " + String.valueOf(request != null));
        } else {
            LOGGER.log(Level.SEVERE, "Cannot create a request since the profile is null.");
            throw new IllegalArgumentException("Profile can't be null.");
        }
    }

    public RequestSignalMessage createSignalMessage(String pullAction, String pullAgreementRef, String pullAgreementRefType,
                                                    String pullConversationId, String pullRefToMessageId, String pullServiceName, String pullServiceType) throws STMException, FileNotFoundException {

        if (this.request != null) {
            RequestSignalMessage requestSignalMessage = this.request.getSignalMessage();
            if (requestSignalMessage != null) {
                LOGGER.log(Level.INFO, "Configuring request signal message as required by message exchange pattern.");
                PullRequest pullRequest = requestSignalMessage.getPullRequest();
                pullRequest.setActionName(pullAction);
                pullRequest.setAgreementRef(pullAgreementRef, pullAgreementRefType);
                pullRequest.setConversationId(pullConversationId);
                pullRequest.setRefToMessageId(pullRefToMessageId);
                pullRequest.setService(pullServiceName, pullServiceType);

                if (secureMessagingFlag) {
                    requestSignalMessage.setSAMLToken(securityCheck.getSecurityTokenAsString());
                }

            } else {
                LOGGER.log(Level.INFO, "Skipping request signal message since it is not allowed by message exchange pattern.");
            }
            return requestSignalMessage;
        } else {
            LOGGER.log(Level.INFO, "Request variable is null, therefore cannot create a signal message.");
            return null;
        }
    }

//    public void constructPing(String service, String action){
//        if (this.request != null) {
//            RequestUserMessage requestUserMessage = this.request.getUserMessage();
//            if (requestUserMessage != null) {
//
//                requestUserMessage.
//            } else {
//                LOGGER.log(Level.INFO, "Skipping request user message since it is not allowed by message exchange pattern.");
//            }
//        } else {
//            LOGGER.log(Level.INFO, "Request variable is null, therefore cannot create a user message.");
//        }
//    }

    public void constructUserMessage(String payloadPath, String payloadContentType, String payload, String amplifyPayload, String attachmentPath) throws Exception {
        if (this.request != null) {
            RequestUserMessage requestUserMessage = this.request.getUserMessage();
            if (requestUserMessage != null) {
                LOGGER.log(Level.INFO, "Configuring request user message as required by message exchange pattern.");
                RequestPart payloadPart = requestUserMessage.createPart();

                if (StringUtils.isNotBlank(payloadContentType)) {
                    payloadPart.setContentType(payloadContentType);
                } else {
                    payloadPart.setContentType("text/xml");
                }
                if (payloadPath != null && doesAFileExistAsAResource(payloadPath)) {
                    LOGGER.log(Level.INFO, "Building payload part.");
                    payloadPart.setInputStream(getResourceAsInputStream(payloadPath));
                    payloadPart.setProperty("PayloadPartProp1", "PayloadPartValue1");
                    payloadPart.setProperty("PayloadPartProp2", "PayloadPartValue2");
                    payloadPart.setSchemaLocation("http://schemaLocation/schema.xsd");
                    payloadPart.setSchemaVersion("2.0");
                    requestUserMessage.setPayloadPart(payloadPart);
                } else if (payload != null) {
                    LOGGER.log(Level.INFO, "Building payload part.");

                    if (amplifyPayload != null) {
                        int amplifyPayloadNum = Integer.parseInt(amplifyPayload);
                        LOGGER.log(Level.INFO, "The amplifyPayload number is " + amplifyPayloadNum);
                        StringBuilder amplifiedPayload = new StringBuilder();
                        while (amplifyPayloadNum > 0) {
                            amplifiedPayload.append(payload);
                            amplifyPayloadNum--;
                        }

                        payloadPart.setData(amplifiedPayload.toString());
                    } else {
                        payloadPart.setData(payload);
                    }
                    LOGGER.log(Level.INFO, "Successfully set the payload.");

                } else {
                    LOGGER.log(Level.INFO, "Skipping payload part since non specified.");
                }

                requestUserMessage.setPayloadPart(payloadPart);

                if (secureMessagingFlag) {
                    requestUserMessage.setSAMLToken(securityCheck.getSecurityTokenAsString());
                }

                requestUserMessage.setMessageProperty("Name1", "Value1");
                requestUserMessage.setMessageProperty("Name2", "Value2");

                if (attachmentPath != null && doesAFileExistAsAResource(attachmentPath)) {
                    LOGGER.log(Level.INFO, "Building attachment part.");
                    RequestPart attachmentPart = requestUserMessage.createPart();
                    attachmentPart.setContentID("content1");
                    attachmentPart.setContentType("text/plain");
                    attachmentPart.setOriginalFileName("Part1");
                    attachmentPart.setInputStream(getResourceAsInputStream(attachmentPath));
                    attachmentPart.setProperty("AttachmentPartProp1", "AttachmentPartValue1");
                    attachmentPart.setProperty("AttachmentPartProp2", "AttachmentPartValue2");
                    attachmentPart.setSchemaLocation("http://schemaLocation/schema.xsd");
                    attachmentPart.setSchemaVersion("2.0");
                    requestUserMessage.addAttachmentPart(attachmentPart);
                } else {
                    LOGGER.log(Level.INFO, "Skipping attachment part since none specified.");
                }

                requestUserMessage.setActionName("action");
//                requestUserMessage.setService("ATO", "OneWayPushBasic"); // don't need since we are setting in exchange profile
                requestUserMessage.setConversationId(UUID.randomUUID().toString());
            } else {
                LOGGER.log(Level.INFO, "Skipping request user message since it is not allowed by message exchange pattern.");
            }
        } else {
            LOGGER.log(Level.INFO, "Request variable is null, therefore cannot create a user message.");
        }
    }

    // Ignore for now
    public RequestUserMessage createUserMessage() {
        if (this.request != null) {
            RequestUserMessage requestUserMessage = this.request.getUserMessage();
            if (requestUserMessage != null) {
                this.requestUserMessage = requestUserMessage;
                setRequestHeader();
                setRequestProperties();
                return this.requestUserMessage;
            } else {
                LOGGER.log(Level.INFO, "Skipping request user message since it is null.");
                return null;
            }
        } else {
            LOGGER.log(Level.INFO, "Request variable is null, therefore cannot create a user message.");
            return null;
        }
    }

    public Request getRequest() {
        return this.request;
    }

    public Request getRequest(String requestBasePath, String overrideOwnerOrgName,
                              String overrideOwnerOrgRole, String overrideOwnerOrgBusinessId,
                              String overrideOwnerOrgBusinessIdType, AS4Client client)
            throws AS4ClientException {

        LOGGER.log(Level.INFO, "RequestFactory getRequest called at -> " + LocalDateTime.now());

        // Load request Properties file
        String requestPropertyFilePath = requestBasePath + "/request.properties";
        propsLoader = new PropertiesLoader(requestPropertyFilePath);

        if (propsLoader.propertiesExistAndNotEmpty()) {
            LOGGER.log(Level.INFO, "Loading request property file from " + requestPropertyFilePath);
        } else {
            LOGGER.log(Level.INFO, "No request property file found under this location " + requestPropertyFilePath + "or the property file is empty ");
        }

        // Retrieve profile information using the file name for the exchange profile
        profile = client.getProfile(profileName);
        if (profile != null) {
            LOGGER.log(Level.INFO, "Profile got from the AS4Client is not null");
            // If there are notNull values passed to the request override the existing in the profile
            overrideOwnerOrgName = (overrideOwnerOrgName == null) ? profile.getOwnerName() : overrideOwnerOrgName;
            overrideOwnerOrgRole = (overrideOwnerOrgRole == null) ? profile.getOwnerRole() : overrideOwnerOrgRole;
            overrideOwnerOrgBusinessId = (overrideOwnerOrgBusinessId == null) ? profile.getOwnerBusinessId() : overrideOwnerOrgBusinessId;
            overrideOwnerOrgBusinessIdType = (overrideOwnerOrgBusinessIdType == null) ? profile.getOwnerBusinessIdType() : overrideOwnerOrgBusinessIdType;

            profile.setOwner(overrideOwnerOrgName, overrideOwnerOrgRole,
                    overrideOwnerOrgBusinessId, overrideOwnerOrgBusinessIdType);

            // Create an instance of ebMS3 request using the Profile Object
            request = client.createRequest(profile);
            // Retrieve the UserMessage element from the ebMS3 request
            requestUserMessage = request.getUserMessage();

            LOGGER.log(Level.INFO, "Interaction Type pattern is -> " + profile.getPattern());
            LOGGER.log(Level.INFO, "Don't set request headers and properties when pattern equals to -> " + Constants.MEP_ONE_WAY_PULL);
            if (!profile.getPattern().equals(Constants.MEP_ONE_WAY_PULL)) {
                setRequestHeader();
                setRequestProperties();
            }
            return request;
        } else {
            LOGGER.log(Level.SEVERE, "Unable to load the exchange profile: " + profileName + "at: " + LocalDateTime.now());
            return null;
        }
    }

//    public void setProfile(Profile profile) {
//        if (checkIfProfileExists()) {
//            LOGGER.log(Level.INFO, "Profile already exists");
//        } else {
//            this.profileName = profile.getName();
//            this.profile = profile;
//        }
//    }

    private void setRequestHeader() {
        // Set corresponding values in the CollaborationInfo section of ebMS3 header
        LOGGER.log(Level.INFO, "Setting request's headers");
        this.requestUserMessage.setActionName(propsLoader.props
                .getProperty("ActionName"));
        this.requestUserMessage.setService(
                propsLoader.props.getProperty("ServiceName"), null);
        this.requestUserMessage.setConversationId(UUID.randomUUID().toString());
    }

    private void setRequestProperties() {
        // Set values in the MessageProperties section of ebMS3 header
        LOGGER.log(Level.INFO, "Setting request's properties");
        List<String> excludedProperties = Arrays.asList("ActionName", "ServiceName");
        List<String> propertyKeys = propsLoader.getPropertiesList(excludedProperties);
        for (String key : propertyKeys) {
            this.requestUserMessage.setMessageProperty(key, propsLoader.props.getProperty(key));
        }
        this.requestUserMessage.setMessageProperty("ReferenceClientVersion", REFERENCE_CLIENT_VERSION);
    }


//    public void setOwnerDetails(String overrideOwnerOrgName, String overrideOwnerOrgRole, String overrideOwnerOrgBusinessId, String overrideOwnerOrgBusinessIdType) {
//        this.overrideOwnerOrgName = (overrideOwnerOrgName == null) ? profile.getOwnerName() : overrideOwnerOrgName;
//        this.overrideOwnerOrgRole = (overrideOwnerOrgRole == null) ? profile.getOwnerRole() : overrideOwnerOrgRole;
//        this.overrideOwnerOrgBusinessId = (overrideOwnerOrgBusinessId == null) ? profile.getOwnerBusinessId() : overrideOwnerOrgBusinessId;
//        this.overrideOwnerOrgBusinessIdType = (overrideOwnerOrgBusinessIdType == null) ? profile.getOwnerBusinessIdType() : overrideOwnerOrgBusinessIdType;
//    }


    public Profile getProfile() {
        return this.profile;
    }

    public String getProfileName() {
        return this.profileName;
    }

//    private boolean checkIfProfileExists() {
//        if (profile != null) {
//            LOGGER.log(Level.INFO, "AS4Client exists.");
//            return true;
//        } else {
//            LOGGER.log(Level.INFO, "AS4Client doesn't exist.");
//            return false;
//        }
//    }

    private void setProfileTransportRetries() {
        // To change values
        profile.setTransportRetries(Integer.parseInt("3"), Integer.parseInt("40"));
    }
}

