package taxationService.message;

public enum AttachmentType {
    BBRP("BBRP"), SRP("SRP");
    String attachType;

    AttachmentType(String attType) {
        this.attachType = attType;
    }

    public static AttachmentType fromString(String text){
        if (text != null) {
            for (AttachmentType type : AttachmentType.values()) {
                if (text.equalsIgnoreCase(type.attachType)) {
                    return type;
                }
            }
        }
        return null;
    }
}
