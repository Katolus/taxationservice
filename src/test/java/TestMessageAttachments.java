import com.ibm.b2b.as4.client.Request;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import taxationService.message.AttachmentType;
import taxationService.message.InteractionType;
import taxationService.message.MessageAttachments;
import taxationService.util.PropertiesLoader;

import java.io.FileNotFoundException;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestMessageAttachments {


    private static MessageAttachments msgAttachments;
    private static Request requestMessage;
    private static final String attachmentsDirectory = "attachments";
    private static PropertiesLoader SRPAttachmentProp = null;
    private static PropertiesLoader BBRPAttachmentProp = null;



    @BeforeClass
    public static void loadMessageAttachmentClass() {
        final String attachmentBBRPPropsPath = attachmentsDirectory + "/config/BBRP.properties";
        final String attachmentSRPPropsPath = attachmentsDirectory + "/config/SRP.properties";
        InteractionType interactionTyp_TWO_WAY_SYNC = InteractionType.TWO_WAY_SYNC;
        msgAttachments = new MessageAttachments(attachmentsDirectory, interactionTyp_TWO_WAY_SYNC);
        BBRPAttachmentProp = new PropertiesLoader(attachmentBBRPPropsPath);
        SRPAttachmentProp = new PropertiesLoader(attachmentBBRPPropsPath);
    }

    @Ignore("Not ready yet")
    @Test
    public void testAssembleSRPRequest() throws FileNotFoundException {
        msgAttachments.attachParts(requestMessage, AttachmentType.BBRP);
    }

    @Test
    public void testSRPPropertiesFilesHasRequiredFields() {

    }

    @Test
    public void testBBRPPropertiesFilesHasRequiredFields() {

    }

    @Test
    public void testIfBBRPRequestPartContentTypeIsParseable() {
        String contentType = BBRPAttachmentProp.getPropertiesList(null).stream().filter(property -> "ContentType".equals(property)).findFirst().get();
        assertThat("Validate that BBRP property file has a parseable content type", BBRPAttachmentProp.props.getProperty(contentType), anyOf(equalTo("text/xml"), equalTo("text/plain"), equalTo("text/html")));
    }

    @Test
    public void testIfSRPRequestPartContentTypeIsParseable() {
        String contentType = SRPAttachmentProp.getPropertiesList(null).stream().filter(property -> "ContentType".equals(property)).findFirst().get();
        assertThat("Validate that SRP property file has a parseable content type", SRPAttachmentProp.props.getProperty(contentType), anyOf(equalTo("text/xml"), equalTo("text/plain"), equalTo("text/html")));
    }
}
