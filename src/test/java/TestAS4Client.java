import com.ibm.b2b.as4.client.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import taxationService.message.AttachmentType;
import taxationService.message.InteractionType;
import taxationService.message.MessageAttachments;
import taxationService.message.RequestFactory;

import java.io.FileNotFoundException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAS4Client {

    private static final String profileName = "SingleSync-EVTE";
    // Paths can be Path not String
    private static final String dataDirectory = "data";
    private static final String configDirectory = "config";
    private static final String auditDirectory = "audit";
    private final String attachmentsDirectory = "attachments";
    private static final String requestBasePath = "requests";
    // for testRequestMessageCreation
    private static final String mainKeyStoreParaphrase = "IGt4a_?6Yui77qwss";
    private static final String overrideOwnerOrgName = "Katoliks";
    private static final String overrideOwnerOrgRole = "http://sbr.gov.au/ato/Role/Registered Agent";
    private static final String overrideOwnerOrgBusinessId = "17801003";
    private static final String overrideOwnerOrgBusinessIdType = "http://ato.gov.au/PartyIdType/TAN";

    private static AS4Client as4Client = null;
    static RequestFactory requestFactory = null;
    static Request requestMessage = null;
    InteractionType interactionType = null;

    @BeforeClass
    public static void loadAS4Client() throws AS4ClientException {
        System.out.println("Config directory -> " + configDirectory);
        System.out.println("Data directory -> " + dataDirectory);
        System.out.println("Audit directory -> " + auditDirectory);
        System.out.println("Request directory -> " + requestBasePath);

        // Use a default *.xml file from the config to jumpstart xml file
        as4Client = AS4ClientFactory.getClient(configDirectory, dataDirectory, mainKeyStoreParaphrase, auditDirectory);
        requestFactory = new RequestFactory(profileName);
        requestMessage = requestFactory.getRequest(requestBasePath, overrideOwnerOrgName, overrideOwnerOrgRole,
                overrideOwnerOrgBusinessId, overrideOwnerOrgBusinessIdType, as4Client);

        assertTrue("Validate that the RequestFactory has been created", requestFactory.getProfileName() == profileName);

        assertThat("Validate that the AS4Client has been created an no dependency is leaking", as4Client, notNullValue());
        assertThat("Validate that the Request Message has been created an no dependency is leaking", requestMessage, notNullValue());
    }

    @Test
    public void testInteractionType(){
        InteractionType interactionType = InteractionType.fromString(requestFactory.getProfile().getPattern());
        assertThat("Validate that InteractionType has been created", interactionType.getInteractionType(), notNullValue());
    }


    @Test
    public void testMessageAttachments() throws FileNotFoundException {
        MessageAttachments msgAttachments = new MessageAttachments(attachmentsDirectory, interactionType);
        assertNotNull("Validate that message attachments has been created", msgAttachments);

        AttachmentType attachmentType = AttachmentType.fromString("BBRP");
        msgAttachments.attachParts(requestMessage, attachmentType);
    }
}
