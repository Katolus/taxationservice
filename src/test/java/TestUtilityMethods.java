import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;
import sun.misc.IOUtils;
import taxationService.TaxationClient;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static taxationService.util.Utility.*;

public class TestUtilityMethods {

    final String existingFileAsResource = "config/Taxation.properties";
    final String existingFileAsPath = "config/client.properties";
    final String nonExistingFile = "config/NonExisting/path";

    static final String messageIDFilePath = "config/async_ids/dummyFile";

    @Test
    public void testDoesAFileUnderAPathExist(){
        assertThat("Check if an existing file can be found", doesAFileUnderAPathExist(existingFileAsPath) ,is(true));
    }

    @Test
    public void testDoesAFileExistAsAResource(){
        assertThat("Check if an existing resource can be found", doesAFileExistAsAResource(existingFileAsResource), is(true));
    }

    @Test
    public void testDoesAFileUnderAPathDoesNotExist(){
        assertThat("Check if an non existing file can be found", doesAFileUnderAPathExist(nonExistingFile), is(false));
    }

    @Test
    public void testDoesAFileExistAsADoesNotResource(){
        assertThat("Check if an non existing resource can be found", doesAFileExistAsAResource(nonExistingFile), is(false));
    }

    @Test
    public void testGetResourceAsInputStream(){
        assertThat("Check if an existing resource generates a not empty input stream" , getResourceAsInputStream(existingFileAsResource), notNullValue());
    }

    @Test
    public void testGetInputStreamAsString() throws IOException {
        assertThat("Check if an stream is translated to String", getInputStreamAsString(getResourceAsInputStream(existingFileAsResource)), instanceOf(String.class));
    }

    @Test
    public void testReadFile2String() throws IOException {
        String messageId = readFile2String(messageIDFilePath);
        assertThat("Reading from the file to String is not null", messageId, notNullValue());
    }

    @Test
    public void testWriteString2File() throws IOException {
        String writeMessage = "Some message Id";
        writeString2File(writeMessage, messageIDFilePath);
        assertThat("Dummy file is not empty", readFile2String(messageIDFilePath) , is(equalTo(writeMessage)));
    }
}
