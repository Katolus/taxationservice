import org.junit.BeforeClass;
import org.junit.Test;
import taxationService.message.MessagePayload;

public class TestMessagePayload {


    static MessagePayload existingResource=null;
    static MessagePayload existingPath =null;
    static MessagePayload nonExisting =null;

    @BeforeClass
    public static void loadMessageAttachmentClass() {
        final String existingFileAsResource = "config/Taxation.properties";
        final String existingFileAsPath = "config/client.properties";
        final String nonExistingFile = "config/NonExisting/path";

        existingResource = new MessagePayload(existingFileAsResource);
        existingPath = new MessagePayload(existingFileAsPath);
        nonExisting = new MessagePayload(nonExistingFile);

    }

    @Test
    public void testAddExistingResourcePayload(){
//        existingResource.addPayload();
    }

    @Test
    public void testAddExistingPathPayload(){

    }

    @Test
    public void testAddNotExistingPayload(){

    }
}
