import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import taxationService.TaxationClient;
import taxationService.message.AttachmentType;
import taxationService.util.PropertiesLoader;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class TestTaxationClient {

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();
    private static PropertiesLoader propertiesLoader;
    private static List<String> requestProperties;
    @BeforeClass
    public static void loadProperties(){
        propertiesLoader = new PropertiesLoader("config/Taxation.properties");
        requestProperties = new PropertiesLoader("requests/request.properties").getPropertiesList(null);
    }

    @Test
    public void testPropertiesLoaderCreation(){
        assertThat("Properties Loader is not empty", propertiesLoader.getPropertiesList(null), hasItems("Resend-Interval", "Resend-Interval-Units","AusKeystore-Location", "Saml-Valid-For", "Service_Endpoint"));
    }

    @Test
    public void testRequestPropertiesHasAllRequiredFields(){
        assertThat("Request.properties file has all mandatory field included", requestProperties, hasItems("ActionName", "ServiceName"));
    }

    @Test
    public void testValidAttachmentType() {
        assertThat("BBRP is a valid attachment type", AttachmentType.valueOf("BBRP"), is(notNullValue()));
    }
    @Test
    public void testInvalidValidAttachmentType(){
        exceptionGrabber.expect(IllegalArgumentException.class);
        AttachmentType.valueOf("CRAZY");
    }




}
