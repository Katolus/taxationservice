import au.gov.abr.securitytokenmanager.SecurityToken;
import au.gov.abr.securitytokenmanager.SecurityTokenManagerClient;
import au.gov.abr.securitytokenmanager.exceptions.STMException;
import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import taxationService.TaxationClient;
import taxationService.security.AUSKeyHandler;
import taxationService.util.PropertiesLoader;
import taxationService.util.Utility;

import java.io.*;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class TestAUSKeyHandler {

    private static AUSKeyHandler ausKeyHandler;
    private static PropertiesLoader refClientPropsLoader;
    private final String certificateAlias = "ABRD:98089681872_RevokedDevice";
    private final String vanguardServiceType = "mock";

    @BeforeClass
    public static void testAUSKeyClient() {
        String ausKeyStorePassword = "Password1!";
        refClientPropsLoader = new PropertiesLoader("config/Taxation.properties");
        String ausKeyPropsFile = refClientPropsLoader.props.getProperty("AusKeystore-Location");
        ausKeyHandler = new AUSKeyHandler(ausKeyStorePassword, ausKeyPropsFile);
    }

    @Test
    public void testAUSKeyCertificate(){
        X509Certificate[] certChain = ausKeyHandler.getCertificateChain(certificateAlias);
        assertNotNull("Assert whether the certificate chain can be retrieved from the AUSKey handler", certChain.length);
    }

    @Test
    public void testAUSKeyPrivateKey(){
        PrivateKey privateKey = ausKeyHandler.getPrivateKey(certificateAlias);
        assertFalse("Assert whether the private key can be retrieved from the AUSKey handler", privateKey.isDestroyed());
        assertThat("Assert that the private key is encrypted in either RSA or DSA digital signatures", privateKey.getAlgorithm(), anyOf(containsString("RSA"), containsString("DSA")));
        assertThat("Assert that the private key's returned format is PKCS#8", privateKey.getFormat(), CoreMatchers.is("PKCS#8"));
    }
    // There is no SAML token to test this. When obtaining one, configuration variable under securitytokenmanager.properties -> au.gov.abr.securitytokenmanager.serialisetoken.filename
    @Ignore("not ready yet")
    @Test
    public void testSecurityTokenManagerConnection() throws STMException, IOException {
        String endpointUrl = "https://thirdparty.authentication.business.gov.au/R3.0/vanguard/S007v1.2/service.svc";
        X509Certificate[] certChain = ausKeyHandler.getCertificateChain(certificateAlias);
        PrivateKey privateKey = ausKeyHandler.getPrivateKey(certificateAlias);

        Properties properties = new Properties();
        // Needs to be set up to a default VanGuardSTSClient class
        System.setProperty("au.gov.abr.securitytokenmanager.properties", "src/test/resources/config/securitytokenmanager.properties");

        properties.load(new FileInputStream(System.getProperty("au.gov.abr.securitytokenmanager.properties")));
        System.out.println(properties.getProperty("au.gov.abr.securitytokenmanager.impl_class"));

        // Testing read file in VANguardSTSClient class
        System.setProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties", "src/test/resources/config/sts.properties");
        String propertiesFile = System.getProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties", "src/test/resources/config/sts.properties");
        System.out.println(new File(propertiesFile).getAbsolutePath());
        System.out.println(Utility.getParentDirectory(TaxationClient.class));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(propertiesFile)));
        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            System.out.println(currentLine);
        }

        System.out.println(System.getProperty("au.gov.abr.securitytokenmanager.VANguardSTSClient.properties"));
        SecurityTokenManagerClient stmClient = new SecurityTokenManagerClient();
        // Security Creation working

        // Imitating cache variables
        String cacheKey = endpointUrl + null + "au.gov.abr.securitytokenmanager.VANguardSTSClient" + certChain[0].getSerialNumber().toString() + certChain[0].getIssuerDN().getName();
        HashMap<String, SecurityToken> tokenCache = new HashMap<>();
        System.out.println(cacheKey);
        System.out.println(tokenCache.toString());

        stmClient.getSecurityIdentityToken(endpointUrl, null, privateKey, certChain);

    }

    @Ignore("not ready yet")
    @Test
    public void testAUSKeySAMLToken(){
        X509Certificate[] certChain = ausKeyHandler.getCertificateChain(certificateAlias);
        PrivateKey privateKey = ausKeyHandler.getPrivateKey(certificateAlias);
        SecurityToken SAMLToken = ausKeyHandler.getSAMLToken(certificateAlias, certChain, privateKey, vanguardServiceType, refClientPropsLoader.props.getProperty("Service_Endpoint"));
        try {
            System.out.println(SAMLToken.getAssertionAsString());
        } catch (STMException e) {
            System.out.println("Unable to retrieve SAML token from STS Response at: " + LocalDateTime.now());
            e.printStackTrace();
        }
    }
}
