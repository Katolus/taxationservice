import org.junit.Test;
import taxationService.util.PropertiesLoader;

import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

public class TestPropertiesLoader {

    private final String taxationServiceProperties = "config/Taxation.properties";
    private final String requestPropertiesPath = "requests";

    @Test
    public void testConfigurationFilesFromString(){
        PropertiesLoader clientProperties= new PropertiesLoader(taxationServiceProperties);
        assertTrue("Client properties requested by String are loadable", clientProperties.propertiesExistAndNotEmpty());
        PropertiesLoader requestProperties = new PropertiesLoader(requestPropertiesPath + "/request.properties");
        assertTrue("Request properties requested by String are loadable", requestProperties.propertiesExistAndNotEmpty());
    }

    @Test
    public void testConfigurationFilesFromPath(){
        PropertiesLoader clientProperties= new PropertiesLoader(Paths.get("config/Taxation.properties"));
        assertTrue("Client properties requested by Path are loadable", clientProperties.propertiesExistAndNotEmpty());
        PropertiesLoader requestProperties = new PropertiesLoader(Paths.get(requestPropertiesPath + "/request.properties"));
        assertTrue("Request properties requested by Path are loadable", requestProperties.propertiesExistAndNotEmpty());
    }
}
